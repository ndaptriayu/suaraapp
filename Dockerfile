FROM php:7.4.10-apache
RUN apt-get update && apt-get install -y libmcrypt-dev && docker-php-ext-install pdo pdo_mysql mysqli
RUN a2enmod rewrite
ADD . /var/www/html
RUN chown -R www-data:www-data /var/www/html
