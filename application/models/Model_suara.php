<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_suara extends CI_model
{

  public function get_all()
  {
    $query = $this->db->select("*")
      ->from('suara')
      ->order_by('id_suara', 'ASC')
      ->get();
    return $query->result();
  }

	public function nik_tps()
	{
		$query = $this->db->select("*")
			->from('pemilih')
			->join('kecamatan','kecamatan.id_kecamatan=pemilih.id_kecamatan')
			->join('keldes','keldes.id_keldes=pemilih.id_keldes')
			->join('tps','tps.id_tps=pemilih.id_tps')
			->order_by('nik', 'ASC')
			->get();
		return $query->result();
	}

	public function get_nik()
	{
		$query = $this->db->select("*")
			->from('pemilih')
			->join('kecamatan','kecamatan.id_kecamatan=pemilih.id_kecamatan')
			->join('keldes','keldes.id_keldes=pemilih.id_keldes')
			->order_by('nik', 'ASC')
			->get();
		return $query->result();
	}

	function tampil_dropdown()
	{

		$query = $this->db->select("*")
		->from('keldes')
		->order_by('id_keldes', 'ASC')
		->get();
		return $query;
	}

	function tampil_dapil()
	{

		$query = $this->db->select("*")
		->from('keldes')
		->join('kecamatan','kecamatan.id_kecamatan=keldes.id_kecamatan')
		->join('dapil','dapil.id_dapil=kecamatan.id_dapil')
		->group_by('keldes.id_keldes', 'ASC')
		->get();
		return $query;
	}

	function tampil_data_chained($id)
	{
		$query = $this->db->query("SELECT * FROM keldes JOIN kecamatan ON keldes.id_kecamatan=kecamatan.id_kecamatan where id_keldes = '$id'");
		return $query;
	}
	function tampil_data_chained2($id) //menampilkan dropdown kedua
	{
		$query = $this->db->query("SELECT * FROM tps JOIN dpt ON tps.id_tps=dpt.id_tps JOIN keldes ON keldes.id_keldes=dpt.id_keldes JOIN kecamatan ON keldes.id_kecamatan=kecamatan.id_kecamatan where keldes.id_keldes = '$id'");
		return $query;
	}

  function getTotalDpt(){
    $query = $this->db->query("SELECT sum(a.total_dpt) as total_dpt from (SELECT ab.* FROM dpt ab join suara ac on ac.id_kecamatan = ab.id_kecamatan and ac.id_keldes = ab.id_keldes and ac.id_tps = ab.id_tps) a");
    return $query->result();
  }

    function get_j(){
	    $query = $this->db->select("*")
        ->from('suara')
		->join('kecamatan','kecamatan.id_kecamatan=suara.id_kecamatan')
		->join('keldes','keldes.id_keldes=suara.id_keldes')
		->join('tps','tps.id_tps=suara.id_tps')
		->order_by('nama_kecamatan', 'ASC')
		->order_by('nama_keldes', 'ASC')
		->order_by('nama_tps', 'ASC')
        ->get();
	return $query->result();
	}

  function get_dpt_byparam($id_kecamatan, $id_keldes, $id_tps){
    $query = $this->db->select("*")
        ->from('dpt')
    		->where('id_kecamatan', $id_kecamatan)
        ->where('id_keldes', $id_keldes)
        ->where('id_tps', $id_tps);
    	return $query->get()->result();
  }

	function get_j_dapil($dapil){
		$query = $this->db->select("*")
        ->from('suara')
		->join('kecamatan a','a.id_kecamatan=suara.id_kecamatan')
		->join('keldes b','b.id_keldes=suara.id_keldes')
		->join('tps c','c.id_tps=suara.id_tps')
		->get_where('kecamatan', array('a.dapil'=>urldecode($dapil)));
	return $query->result();
	}

	//start per dapil
	function tampil_dropdowndapil() //menampilkan dropdown utama
	{
		$query = $this->db->select("*")
		->from('keldes')
		->join('kecamatan','kecamatan.id_kecamatan=keldes.id_kecamatan')
		->join('dapil','dapil.id_dapil=kecamatan.id_dapil')
		->join('tbl_user','tbl_user.level_dapil = dapil.id_dapil')
		->join('tbl_adm','tbl_adm.id_pengguna = tbl_user.id_pengguna')
		->order_by('keldes.nama_keldes', 'ASC')
		->get();
		return $query;
	}
	function get_d(){
	    $query = $this->db->select("*")
            ->from('suara')
			->join('kecamatan','kecamatan.id_kecamatan=suara.id_kecamatan')
			->join('keldes','keldes.id_keldes=suara.id_keldes')
			->join('tps','tps.id_tps=suara.id_tps')
			->join('dapil','dapil.id_dapil=kecamatan.id_dapil')
			->join('tbl_user','tbl_user.level_dapil = dapil.id_dapil')
			->join('tbl_adm','tbl_adm.id_pengguna = tbl_user.id_pengguna')
			->order_by('nama_kecamatan', 'ASC')
			->order_by('nama_keldes', 'ASC')
			->order_by('nama_tps', 'ASC')
            ->get();
        return $query->result();
	}
	//end perdapil
  public function get_kecamatan()
  {
    $query = $this->db->select("*")
      ->from('kecamatan')
      ->order_by('id_kecamatan', 'ASC')
      ->get();
    return $query->result();
  }
  public function get_keldes()
  {
    $query = $this->db->select("*")
      ->from('keldes')
      ->order_by('id_keldes', 'ASC')
      ->get();
    return $query->result();
  }
  public function get_tps()
  {
    $query = $this->db->select("*")
      ->from('tps')
      ->order_by('id_tps', 'ASC')
      ->get();
    return $query->result();
  }

  function get_tpsdesa() //menampilkan dropdown utama
	{
		$query = $this->db->query("SELECT
		keldes.nama_keldes,
		kecamatan.nama_kecamatan,
		tps.nama_tps,
		dpt.id_tps,
		pemilih.id_kecamatan,
		pemilih.nik,
		tbl_adm.id_pengguna,
		dpt.id_keldes
		FROM
		keldes
		INNER JOIN kecamatan ON keldes.id_kecamatan = kecamatan.id_kecamatan
		INNER JOIN dpt ON keldes.id_keldes = dpt.id_keldes
		INNER JOIN tps ON dpt.id_tps = tps.id_tps
		INNER JOIN pemilih ON keldes.id_keldes = pemilih.id_keldes
		INNER JOIN tbl_adm ON pemilih.nik = tbl_adm.id_pengguna
		ORDER BY
		tps.nama_tps ASC
		");
		return $query;
	}

  public function get_tpsd()
  {
    $query = $this->db->select("*")
      ->from('tps')
	  ->join('pemilih','tps.id_tps=pemilih.id_tps')
      ->group_by('tps.id_tps', 'ASC')
      ->get();
    return $query->result();
  }

  public function simpan($data)
  {
    $query = $this->db->insert("suara", $data);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }

  public function edit($idjb)
  {
    $query = $this->db->where("id_suara", $idjb)
      ->get("suara");
    if ($query) {
      return $query->row();
    } else {
      return false;
    }
  }

  public function update($data, $id)
  {
    $query = $this->db->update("suara", $data, $id);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }

  public function hapus($id)
  {
    $query = $this->db->delete("suara", $id);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }
} // END OF class Model_suara
