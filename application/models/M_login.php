<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_login extends CI_Model
{
    public function get_all()
    {
        $query = $this->db->select("*")
            ->from('tbl_adm')
            ->order_by('id_user', 'ASC')
            ->get();
        return $query->result();
    }
	
	
	function tampil_join(){
	    $query = $this->db->select("*")
            ->from('pemilih')
            ->join('tbl_adm','tbl_adm.id_pengguna=pemilih.nik')
			->join('kecamatan','kecamatan.id_kecamatan=pemilih.id_kecamatan')
			->join('keldes','keldes.id_keldes=pemilih.id_keldes')
            ->get();
        return $query->result();
	}
	
	public function get_kecamatan()
    {
        $query = $this->db->select("*")
            ->from('kecamatan')
            ->order_by('id_kecamatan', 'DESC')
            ->get();
        return $query->result();
    }
	public function get_keldes()
    {
        $query = $this->db->select("*")
            ->from('keldes')
            ->order_by('id_keldes', 'DESC')
            ->get();
        return $query->result();
    }
	public function get_tps()
    {
        $query = $this->db->select("*")
            ->from('tps')
            ->order_by('id_tps', 'DESC')
            ->get();
        return $query->result();
    }

    function cek_login($table, $where)
    {
        return $this->db->get_where($table, $where)->row_array();
    }

    public function getp($username, $password)
    {
        $query = $this->db->where("username", $username)
            ->where("password", $password)
            ->get("tbl_adm");
        if ($query) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function simpan($data)
    {
        $query = $this->db->insert("tbl_adm", $data);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function edit($idp)
    {
        $query = $this->db->where("id_pengguna", $idp)
            ->get("tbl_adm");
        if ($query) {
            return $query->row();
        } else {
            return false;
        }
    }
	public function editx($idjx)
	{
		$query = $this->db->where("nik", $idjx)
		  ->get("pemilih");  
		if ($query) {
		  return $query->row();
		} else {
		  return false;
		}
	}
	
    public function update($data, $id)
    {
        $query = $this->db->where("id_user", $id)
            ->update("tbl_adm", $data);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function hapusu($idu)
    {
        $query = $this->db->delete("tbl_adm", $idu);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }
}
