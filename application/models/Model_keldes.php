<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_keldes extends CI_model
{

  public function get_all()
  {
    $query = $this->db->select("*")
      ->from('keldes')
      ->order_by('id_keldes', 'ASC')
      ->get();
    return $query->result();
  }
	
  public function simpan($data)
  {
    $query = $this->db->insert("keldes", $data);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }

  public function hapus($id)
  {
    $query = $this->db->delete("keldes", $id);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }
  
  function get_update($xkode,$xnama){
	$query=$this->db->query("UPDATE keldes SET nama_keldes='$xnama' WHERE id_keldes='$xkode'");
		return $query;
  }
} // END OF class Model_keldes
