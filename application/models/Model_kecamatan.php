<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_kecamatan extends CI_model
{

  public function get_all()
  {
    $query = $this->db->select("*")
      ->from('kecamatan')
      ->order_by('id_kecamatan', 'ASC')
      ->get();
    return $query->result();
  }
	
  public function simpan($data)
  {
    $query = $this->db->insert("kecamatan", $data);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }

  public function hapus($id)
  {
    $query = $this->db->delete("kecamatan", $id);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }
  
  function get_update($xkode,$xnama){
	$query=$this->db->query("UPDATE kecamatan SET nama_kecamatan='$xnama' WHERE id_kecamatan='$xkode'");
		return $query;
  }
} // END OF class Model_kecamatan
