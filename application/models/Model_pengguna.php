<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_pengguna extends CI_Model
{
    
	function tampil_dropdown()
	{

		$query = $this->db->select("*")
		->from('pemilih')
		->order_by('nik', 'ASC')
		->get();
		return $query;
	}
	
	function tampil_data_chained($id)
	{
		$query = $this->db->query("SELECT * FROM pemilih JOIN kecamatan ON pemilih.id_kecamatan=kecamatan.id_kecamatan JOIN dapil ON dapil.id_dapil=kecamatan.id_dapil where nik = '$id'");
		return $query;
	}
	
	public function get_all()
    {
        $query = $this->db->select("*")
            ->from('tbl_adm')
            ->join('pemilih','pemilih.nik=tbl_adm.id_pengguna')
			->join('kecamatan','kecamatan.id_kecamatan=pemilih.id_kecamatan')
			->join('keldes','keldes.id_keldes=pemilih.id_keldes')
            ->get();
        return $query->result();
    }
	
	public function get_op()
    {
        $query = $this->db->query("SELECT * FROM tbl_adm INNER JOIN pemilih ON pemilih.nik=tbl_adm.id_pengguna INNER JOIN kecamatan ON kecamatan.id_kecamatan=pemilih.id_kecamatan INNER JOIN keldes ON keldes.id_keldes=pemilih.id_keldes WHERE tbl_adm.level = 'desa' OR tbl_adm.level = 'kecamatan' OR tbl_adm.level = 'tps'");
        return $query->result();
    }
	
	function get_u(){
	    $query = $this->db->select("*")
            ->from('tbl_user')
            ->join('tbl_adm','tbl_adm.id_pengguna=tbl_user.id_pengguna')
			->join('pemilih','pemilih.nik=tbl_user.id_pengguna')
            ->get();
        return $query->result();
	}
	
	public function get_pengguna() {
		$query = $this->db->select("*")
            ->from('pemilih')
			->join('kecamatan','kecamatan.id_kecamatan=pemilih.id_kecamatan')
			->join('dapil','dapil.id_dapil=kecamatan.id_dapil')
			->order_by('nik', 'ASC')
            ->get();
        return $query->result();
	}
	
	public function get_nik() {
		$query = $this->db->select("*")
		  ->from('pemilih')
		  ->order_by('nik', 'ASC')
		  ->get();
		return $query->result();
	}
	
	function get_d(){
	    $query = $this->db->select("*")
            ->from('pemilih')
			->join('kecamatan','kecamatan.id_kecamatan=pemilih.id_kecamatan')
			->join('keldes','keldes.id_keldes=pemilih.id_keldes')
			->join('dapil','dapil.id_dapil=kecamatan.id_dapil')
			->join('tbl_user','tbl_user.level_dapil = dapil.id_dapil')
			->join('tbl_adm','tbl_adm.id_pengguna = pemilih.nik')
			->order_by('nik', 'ASC')
            ->get();
        return $query->result();
	}
	
	public function get_dapil() {
		$query = $this->db->select("*")
		  ->from('dapil')
		  ->join('kecamatan','kecamatan.id_dapil=dapil.id_dapil')
		  ->group_by('dapil.id_dapil', 'ASC')
		  ->get();
		return $query->result();
	}
	
    function cek_login($table, $where)
    {
        return $this->db->get_where($table, $where)->row_array();
    }

    public function getp($username, $password)
    {
        $query = $this->db->where("username", $username)
            ->where("password", $password)
            ->get("tbl_user");
        if ($query) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function simpan($data)
    {
        $query = $this->db->insert("tbl_user", $data);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }
	
	public function edit_user($idjb)
	{
		$query=$this->db->query("SELECT
		pemilih.nik,
		pemilih.nama_pemilih,
		kecamatan.nama_kecamatan,
		keldes.nama_keldes,
		tbl_user.level_dapil,
		tbl_user.id_pengguna,
		tbl_adm.id_user,
		tbl_adm.username,
		tbl_adm.`level`,
		pemilih.kontak,
		pemilih.`status`,
		pemilih.operator,
		kecamatan.dapil
		FROM
		tbl_adm
		INNER JOIN pemilih ON pemilih.nik = tbl_adm.id_pengguna
		INNER JOIN kecamatan ON pemilih.id_kecamatan = kecamatan.id_kecamatan
		INNER JOIN keldes ON pemilih.id_keldes = keldes.id_keldes
		INNER JOIN tbl_user ON tbl_user.level_dapil = kecamatan.id_dapil
		WHERE pemilih.nik = '$idjb'");
		if ($query) {
		  return $query->row();
		} else {
		  return false;
		}
	}
	public function update_user($data, $id)
    {	
		$query = $this->db->update("tbl_adm", $data, $id);
		if ($query) {
		  return true;
		} else {
		  return false;
		}	
    }
	

    public function editx($idjx)
	{
		$query = $this->db->where("id_pengguna", $idjx)
		  ->get("tbl_user");  
		if ($query) {
		  return $query->row();
		} else {
		  return false;
		}
	}
	
	public function edit($idjb)
	{
		$query = $this->db->where("id_pengguna", $idjb)
		  ->get("tbl_adm");  
		if ($query) {
		  return $query->row();
		} else {
		  return false;
		}
	}

    public function update($data, $id)
    {
        $query = $this->db->where("id_pengguna", $id)
            ->update("tbl_user", $data);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }
	
	public function updatex($data, $id)
    {
        $query = $this->db->where("id_pengguna", $id)
            ->update("tbl_adm", $data);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function hapusu($idu)
    {
        $query = $this->db->delete("tbl_user", $idu);
		$query = $this->db->delete("tbl_adm", $idu);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }
}
