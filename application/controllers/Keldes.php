<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keldes extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('model_keldes');
    $this->load->helper(array('form', 'url'));
	$this->load->library('form_validation');
        if ($this->session->userdata('status') != "loginCOD") {
            redirect(base_url("log-in"));
        }
  }

  public function index()
  {

    $this->load->view('index');
  }

  public function datakeldes()
  {

    $data = array(
      'title' => 'Data Kelurahan/Desa',
      'data_keldes' => $this->model_keldes->get_all(),
      'isi' => 'backend/data_keldes'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  public function tambah()
  {
    $data = array(
      'title' => 'Tambah Kelurahan/Desa',
      'data_jb' => $this->model_keldes->get_all(),
      'isi' => 'backend/tambah_keldes'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  public function simpan()
  {
    $data = array(
      'nama_keldes' => $this->input->post("Txtkeldes")
    );
	
	$nama_keldes = $this->input->post('Txtkeldes');
	$sql = $this->db->query("SELECT nama_keldes FROM keldes where nama_keldes='$nama_keldes'");
	$cek_nik = $sql->num_rows();
	if ($cek_nik > 0) {
	$this->session->set_flashdata('notif', '<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Peringatan! Data Kelurahan/Desa Sudah Ada</div>');
	redirect('keldesfile');
	}else{
    $this->model_keldes->simpan($data);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! Data Kelurahan/Desa Berhasil di Simpan</div>');

    redirect('keldesfile');
	}
  }

  public function hapus($idj)
  {
    $id['id_keldes'] = $this->uri->segment(3);
    $this->model_keldes->hapus($id);
    redirect('keldesfile');
  }
  
  public function update(){
	$xkode	=htmlspecialchars($this->input->post("id", true));
	$xnama	=htmlspecialchars($this->input->post("nama", true));
	$this->model_keldes->get_update($xkode,$xnama);
	redirect('keldesfile');
  }

} // END OF class Kelurahan/Desa
