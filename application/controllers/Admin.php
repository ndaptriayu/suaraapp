<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->model('m_login');
		$this->load->model('model_suara');
		$this->load->helper(array('form', 'url'));
        if ($this->session->userdata('status') != "loginCOD") {
            redirect(base_url("log-in"));
        }
    }

	public function index()
	{
		if ($this->session->userdata('level') == "superadmin") {
		$data = array(
            'title' => 'SIPIL 2020',
            'isi' => 'backend/home',
        );
		$this->load->view('backend/layout/wrapper', $data);
		
		}elseif($this->session->userdata('level') == "operasional") {
		$data = array(
            'title' => 'SIPIL 2020',
            'isi' => 'backend/home',
        );
		$this->load->view('backend/layout/wrapper', $data);
		
		}elseif($this->session->userdata('level') == "desa") {
		$data = array(
            'title' => 'SIPIL 2020',
			'nik' => $this->model_suara->get_nik(),
			'jns_keldes' => $this->model_suara->get_keldes(),
			'jns_tpsdesa' => $this->model_suara->get_tpsdesa(),
            'isi' => 'backend/suara/data_suara',
        );
		$this->load->view('backend/layout/wrapper', $data);
		
		}elseif($this->session->userdata('level') == "tps") {
		$data = array(
            'title' => 'SIPIL 2020',
			'nik' => $this->model_suara->get_nik(),
			'nikpl' => $this->model_suara->nik_tps(),
			'jns_keldes' => $this->model_suara->get_keldes(),
			'jns_tps' => $this->model_suara->get_tps(),
            'isi' => 'backend/suara/data_suara',
        );
		$this->load->view('backend/layout/wrapper', $data);
		
		}elseif($this->session->userdata('level') == "pengawas") {
		$data = array(
            'title' => 'SIPIL 2020',
            'isi' => 'backend/home',
        );
		$this->load->view('backend/layout/wrapper', $data);
		
		}else{
		$this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible" role="alert">Username atau Password Anda Tidak Ditemukan</div>');
		
		redirect(base_url("login"));
		}
	}
}