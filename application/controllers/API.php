<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('model_suara');
	  $this->load->helper(array('url'));
  }

  public function getTotalSuara(){
    if (isset($_SERVER['HTTP_ORIGIN'])){
			header("Access-Control-Allow-Origin: *");
			header('Access-Control-Allow-Credentials: true');
			header('Access-Control-Max-Age: 86400');
			header("Content-type:application/json");
		}

		if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
				header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
				header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

			exit(0);
		}  
    $suaraList = $this->model_suara->get_j();

    $totalSuara = 0;
    $paslon1 = 0;
    $paslon2 = 0;
    $paslon3 = 0;
    $paslon4 = 0;
    $tidaksah = 0;
    $total_dptb = 0;
    $total_dpt = 0;

    $temptotalgolput = 0;
    foreach($suaraList as $row){
      $paslon1 = $paslon1 + $row->paslon1;
      $paslon2 = $paslon2 + $row->paslon2;
      $paslon3 = $paslon3 + $row->paslon3;
      $paslon4 = $paslon4 + $row->paslon4;
      $tidaksah = $tidaksah + $row->tidaksah;
      $total_dptb = $total_dptb + $row->total_dptb;
    }
    $dptList = $this->model_suara->getTotalDpt();
    foreach($dptList as $item){
      $total_dpt = $item->total_dpt;
    }
    $tmpTotal = $paslon1 + $paslon2 + $paslon3 + $paslon4 + $tidaksah;
    $totalSuara = ($total_dptb + $total_dpt);

    $temptotalgolput = ($total_dptb + $total_dpt) - $tmpTotal;

    $dataArray = array(
      'paslon1'=> $paslon1,
      'paslon2'=> $paslon2,
      'paslon3'=> $paslon3,
      'paslon4'=> $paslon4,
      'tidaksah'=> $tidaksah,
      'total_dptb' => $total_dptb,
      'totalSuara'=> $totalSuara,
      'golput'=> $temptotalgolput
    );

    $json = array(
      'status'=> 'success',
      'message'=>'berhasil mengambil data',
      'data'=> $dataArray
    );

    echo json_encode($json, true);
  }

  public function getTotalSuaraByDapil($dapil){
    $suaraList = $this->model_suara->get_j_dapil($dapil);

    $totalSuara = 0;
    $paslon1 = 0;
    $paslon2 = 0;
    $paslon3 = 0;
    $paslon4 = 0;
    $tidaksah = 0;
    foreach($suaraList as $row){
      $paslon1 = $paslon1 + $row->paslon1;
      $paslon2 = $paslon2 + $row->paslon2;
      $paslon3 = $paslon3 + $row->paslon3;
      $paslon4 = $paslon4 + $row->paslon4;
      $tidaksah = $tidaksah + $row->tidaksah;
    }
    $totalSuara = $paslon1 + $paslon2 + $paslon3 + $paslon4 + $tidaksah;

    $dataArray = array(
      'paslon1'=> $paslon1,
      'paslon2'=> $paslon2,
      'paslon3'=> $paslon3,
      'paslon4'=> $paslon4,
      'tidaksah'=> $tidaksah,
      'totalSuara'=> $totalSuara
    );

    $json = array(
      'status'=> 'success',
      'message'=>'berhasil mengambil data',
      'data'=> $dataArray
    );

    echo json_encode($json, true);
  }
}
?>
