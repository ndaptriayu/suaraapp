<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kecamatan extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('model_kecamatan');
    $this->load->helper(array('form', 'url'));
	$this->load->library('form_validation');
        if ($this->session->userdata('status') != "loginCOD") {
            redirect(base_url("log-in"));
        }
  }

  public function index()
  {

    $this->load->view('index');
  }
  
  public function datakecamatan()
  {

    $data = array(
      'title' => 'Data Kecamatan',
      'data_kecamatan' => $this->model_kecamatan->get_all(),
      'filter_kecamatan' => $this->model_kecamatan->get_filter(),
      'isi' => 'backend/data_kecamatan'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  function cetak()
  {
	$this->load->view('backend/laporan/cetak_kecamatan');
  }
  
  public function tambah()
  {
    $data = array(
      'title' => 'Tambah Kecamatan',
      'data_jb' => $this->model_kecamatan->get_all(),
      'isi' => 'backend/tambah_kecamatan'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  public function simpan()
  {
    $data = array(
      'nama_kecamatan' => htmlspecialchars($this->input->post("Txtkecamatan", true))
    );
	
	$nama_kecamatan = $this->input->post('Txtkecamatan');
	$sql = $this->db->query("SELECT nama_kecamatan FROM kecamatan where nama_kecamatan='$nama_kecamatan'");
	$cek_nik = $sql->num_rows();
	if ($cek_nik > 0) {
	$this->session->set_flashdata('notif', '<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Peringatan! Data kecamatan Sudah Ada</div>');
	redirect('kecamatanfile');
	}else{
    $this->model_kecamatan->simpan($data);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! Data kecamatan Berhasil di Simpan</div>');

    redirect('kecamatanfile');
	}
  }

  public function hapus($idj)
  {
    $id['id_kecamatan'] = $this->uri->segment(3);
    $this->model_kecamatan->hapus($id);
    redirect('kecamatanfile');
  }
  
  public function update(){
	$xkode	=htmlspecialchars($this->input->post("id", true));
	$xnama	=htmlspecialchars($this->input->post("nama", true));
	$this->model_kecamatan->get_update($xkode,$xnama);
	redirect('kecamatanfile');
  }

} // END OF class kecamatan
