<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hitungan extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('model_hitungan');
    $this->load->helper(array('form', 'url'));
	$this->load->library('form_validation');
        if ($this->session->userdata('status') != "loginCOD") {
            redirect(base_url("log-in"));
        }
  }

  public function index()
  {
    $this->load->view('index');
  }

  public function total_hitungan()
  {
    $data = array(
      'title' => 'Total Perhitungan Suara',
      'data_suara' => $this->model_hitungan->get_j(),
	  'dropdown' => $this->model_hitungan->tampil_dropdown(),
	  'nik' => $this->model_hitungan->get_nik(),
	  'provinsi' => $this->model_hitungan->provinsi(),
	  'dtps' => $this->model_hitungan->get_tps(),
      'isi' => 'backend/total_suara'
    );
	$this->load->view('backend/layout/wrapper', $data);
  }

  public function datahitungan()
  {
    $data = array(
      'title' => 'Rekap Suara Kecamatan,Kel/Desa, TPS',
      'data_suara' => $this->model_hitungan->get_j(),
	  'data_perdapil' => $this->model_hitungan->get_d(),
	  'dropdowndapil' => $this->model_hitungan->tampil_dropdowndapil(),
	  'dropdown' => $this->model_hitungan->tampil_dropdown(),
	  'nik' => $this->model_hitungan->get_nik(),
	  'provinsi' => $this->model_hitungan->provinsi(),
	  'dtps' => $this->model_hitungan->get_tps(),
      'isi' => 'backend/data_hitungan'
    );
	$this->load->view('backend/layout/wrapper', $data);
  }
  
  public function report()
  {
    $data = array(
      'title' => 'Rekap Suara Kecamatan,Kel/Desa, TPS',
      'data_suara' => $this->model_hitungan->get_j(),
	  'data_perdapil' => $this->model_hitungan->get_d(),
	  'dropdowndapil' => $this->model_hitungan->tampil_dropdowndapil(),
	  'dropdown' => $this->model_hitungan->tampil_dropdown(),
	  'nik' => $this->model_hitungan->get_nik(),
	  'provinsi' => $this->model_hitungan->provinsi(),
	  'dtps' => $this->model_hitungan->get_tps(),
      'isi' => 'backend/laporan/report_hitungan'
    );
	$this->load->view('backend/layout/wrapper', $data);
  }
  public function cetak()
  {
	$this->load->view('backend/laporan/cetak_hitungan');
  }
  
  public function rekapkecamatan()
  {
    $data = array(
      'title' => 'Rekap Suara Kelurahan/Desa',
      'data_suara' => $this->model_hitungan->get_j(),
	  'dropdown' => $this->model_hitungan->tampil_dropdown(),
	  'nik' => $this->model_hitungan->get_nik(),
	  'dkeldes' => $this->model_hitungan->get_keldesk(),
	  'provinsi' => $this->model_hitungan->provinsi(),
  	  'dtps' => $this->model_hitungan->get_tpsk(),
      'isi' => 'backend/rekap_kecamatan'
    );
	$this->load->view('backend/layout/wrapper', $data);
  }
  
  public function rekapkeldes()
  {
    $data = array(
      'title' => 'Rekap Suara Kecamatan, Kelurahan/Desa',
      'data_suara' => $this->model_hitungan->get_j(),
	  'dropdown' => $this->model_hitungan->tampil_dropdown(),
	  'nik' => $this->model_hitungan->get_nik(),
	  'provinsi' => $this->model_hitungan->provinsi(),
	  'dtps' => $this->model_hitungan->get_tps(),
      'isi' => 'backend/rekap_keldes'
    );
	$this->load->view('backend/layout/wrapper', $data);
  }
  
  function ambil_data()
  {
	$modul=$this->input->post('modul');
	$id=$this->input->post('id');
	if($modul=="kabupaten"){
		echo $this->model_hitungan->kabupaten($id);
		}else if($modul=="kecamatan"){
		echo $this->model_hitungan->kecamatan($id);
	}
  }
	
  function tampil_chained()
	{
		$id = $_POST['id'];
		$dropdown_chained = $this->model_hitungan->tampil_data_chained($id);
		foreach ($dropdown_chained->result() as $baris) { //masukan fild id dan nama dari dropdown secondarynya
			echo "<option value='".$baris->nama_kecamatan."'>".$baris->nama_kecamatan."</option>";
		}
	}
} // END OF class Suara