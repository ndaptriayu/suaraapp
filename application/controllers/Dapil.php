<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dapil extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('model_kecamatan');
	$this->load->model('model_keldes');
	$this->load->model('model_dapil');
    $this->load->helper(array('form', 'url'));
	$this->load->library('form_validation');
	if ($this->session->userdata('status') != "loginCOD") {
		redirect(base_url("log-in"));
	}
  }

  public function index()
  {
    $this->load->view('index');
  }

  public function datadapil()
  {
    $data = array(
      'title' => 'Data Dapil',
      'pil' => $this->model_dapil->get_all(),
      'isi' => 'backend/data_dapil'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  function cetak()
  {
	$this->load->view('backend/laporan/cetak_dapil');
  }
} // END OF class kecamatan
