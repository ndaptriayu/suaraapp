<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dpt extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('model_dpt');
    $this->load->helper(array('form', 'url'));
	$this->load->library('form_validation');
        if ($this->session->userdata('status') != "loginCOD") {
            redirect(base_url("log-in"));
        }
  }

  public function index()
  {

    $this->load->view('index');
  }

  public function datadpt()
  {
    $data = array(
      'title' => 'Data Dpt',
      'data_dpt' => $this->model_dpt->get_j(),
	  
	  'data_perdapil' => $this->model_dpt->get_d(),
	  'dropdowndapil' => $this->model_dpt->tampil_dropdowndapil(),
	  
	  'dropdown' => $this->model_dpt->tampil_dropdown(),
	  'nik' => $this->model_dpt->get_nik(),
	  'nikpl' => $this->model_dpt->nik_tps(),
	  'jns_keldes' => $this->model_dpt->get_keldes(),
	  'jns_tps' => $this->model_dpt->get_tps(),
	  'jns_tpsd' => $this->model_dpt->get_tpsd(),
      'isi' => 'backend/data_dpt'
    );
		$this->form_validation->set_rules('id_adm', 'id_adm', 'required|numeric|trim');
		$this->form_validation->set_rules('Txtkecamatan', 'Kecamatan', 'required');
        $this->form_validation->set_rules('Txtkeldes', 'Kelurahan/Desa', 'required');
		$this->form_validation->set_rules('Txttps', 'TPS', 'required');
		$this->form_validation->set_rules('Txtdpt', 'Total DPT', 'required|numeric|trim');

        if ($this->form_validation->run() == false) {
            //GAGAL
            $this->load->view('backend/layout/wrapper', $data);
        } else {
            //BERHASIL
            $this->simpan();
        }
  }
	
	function tampil_chained()
	{
		$id = $_POST['id'];
		$dropdown_chained = $this->model_dpt->tampil_data_chained($id);
		foreach ($dropdown_chained->result() as $baris) {
			echo "<option value='".$baris->id_kecamatan."'>".$baris->nama_kecamatan."</option>";
		}
	}
	function tampil_chained2()
	{
		$id = $_POST['id'];
		$dropdown_chained = $this->model_dpt->tampil_data_chained2($id);
		foreach ($dropdown_chained->result() as $baris) { //masukan fild id dan nama dari dropdown secondarynya
			echo "<option value='".$baris->id_tps."'>".$baris->nama_tps."</option>";
		}
	}
	
  public function simpan()
  {
    $data = array(
	  'id_adm' => htmlspecialchars($this->input->post("id_adm", true)),
	  'id_kecamatan' => htmlspecialchars($this->input->post("Txtkecamatan", true)),
	  'id_keldes' => htmlspecialchars($this->input->post("Txtkeldes", true)),
	  'id_tps' => htmlspecialchars($this->input->post("Txttps", true)),
	  'total_dpt' => htmlspecialchars($this->input->post("Txtdpt", true))  
    );
	
	$data2 = array(
	  'user_input' => htmlspecialchars($this->input->post("id_adm", true)),
	  'id_kecamatan' => htmlspecialchars($this->input->post("Txtkecamatan", true)),
	  'id_keldes' => htmlspecialchars($this->input->post("Txtkeldes", true)),
	  'id_tps' => htmlspecialchars($this->input->post("Txttps", true)),
	  'status' => "Belum Mengisi"
    );
	
	$kecamatan = $this->input->post('Txtkecamatan');
	$keldes = $this->input->post('Txtkeldes');
	$tps = $this->input->post('Txttps');
	$sql = $this->db->query("SELECT id_kecamatan,id_keldes,id_tps FROM dpt where id_kecamatan='$kecamatan' AND id_keldes='$keldes' AND id_tps='$tps'");
	$cek_nik = $sql->num_rows();
	if ($cek_nik > 0) {
	$this->session->set_flashdata('notif', '(Data Sudah Ada). ');
	redirect('dptfile');
	}else{
    $this->model_dpt->simpan($data);
	$this->db->insert('notifikasi',$data2);
    $this->session->set_flashdata('notif', '(Data Tersimpan). ');
    redirect('dptfile');
	}    
  }

  public function hapus($idj)
  {
    $id['id_dpt'] = $this->uri->segment(3);
    $this->model_dpt->hapus($id);
    redirect('dptfile');
  }
  
  public function update(){
	$xkode	=htmlspecialchars($this->input->post("id", true));
	$xnama	=htmlspecialchars($this->input->post("nama", true));
	$this->model_dpt->get_update($xkode,$xnama);
	redirect('dptfile');
  }

} // END OF class dpt
