<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pemilih extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('model_pemilih');
    $this->load->helper(array('form', 'url'));
	$this->load->library('form_validation');
        if ($this->session->userdata('status') != "loginCOD") {
            redirect(base_url("log-in"));
        }
  }

  public function index()
  {

    $this->load->view('index');
  }

  public function datapemilih()
  {
    $data = array(
      'title' => 'Data Pendukung',
      'data_pemilih' => $this->model_pemilih->get_j(),
	  'dropdown' => $this->model_pemilih->tampil_dropdown(),
	  'jenism' => ['pengurus partai', 'saksi', 'pl tps'],
	  
	  'data_perdapil' => $this->model_pemilih->get_d(),
	  'dropdowndapil' => $this->model_pemilih->tampil_dropdowndapil(),
	  
	  'jns_kecamatan' => $this->model_pemilih->get_kecamatan(),
	  'jns_keldes' => $this->model_pemilih->get_keldes(),
	  'jns_tps' => $this->model_pemilih->get_tps(),
      'isi' => 'backend/pemilih/data_pemilih'
    );
		$this->form_validation->set_rules('id_adm', 'id_adm', 'required');
		$this->form_validation->set_rules('Txtnik', 'NIK', 'required|numeric|max_length[16]');
        $this->form_validation->set_rules('Txtpemilih', 'Nama Pemilih', 'required');
		$this->form_validation->set_rules('Txtkecamatan', 'Kecamatan', 'required');
        $this->form_validation->set_rules('Txtkeldes', 'Kelurahan/Desa', 'required');
		$this->form_validation->set_rules('Txttps', 'TPS', 'required');
		$this->form_validation->set_rules('Txtkontak', 'Kontak', 'required');
		$this->form_validation->set_rules('Txtstatus', 'Status', 'required');

        if ($this->form_validation->run() == false) {
            //GAGAL
            $this->load->view('backend/layout/wrapper', $data);
        } else {
            //BERHASIL
            $this->simpan();
        }
  }
	
	function tampil_chained()
	{
		$id = $_POST['id'];
		$dropdown_chained = $this->model_pemilih->tampil_data_chained($id);
		foreach ($dropdown_chained->result() as $baris) { //masukan fild id dan nama dari dropdown secondarynya
			echo "<option value='".$baris->id_kecamatan."'>".$baris->nama_kecamatan."</option>";
		}
	}
	function tampil_chained2()
	{
		$id = $_POST['id'];
		$dropdown_chained = $this->model_pemilih->tampil_data_chained2($id);
		foreach ($dropdown_chained->result() as $baris) { //masukan fild id dan nama dari dropdown secondarynya
			echo "<option value='".$baris->id_tps."'>".$baris->nama_tps."</option>";
		}
	}
	
  public function simpan()
  {
    $data = array(
	  'operator' => htmlspecialchars($this->input->post("id_adm", true)),
	  'nik' => htmlspecialchars($this->input->post("Txtnik", true)),
      'nama_pemilih' => htmlspecialchars($this->input->post("Txtpemilih", true)),
	  'id_kecamatan' => htmlspecialchars($this->input->post("Txtkecamatan", true)),
	  'id_keldes' => htmlspecialchars($this->input->post("Txtkeldes", true)),
	  'id_tps' => htmlspecialchars($this->input->post("Txttps", true)),
	  'kontak' => htmlspecialchars($this->input->post("Txtkontak", true)),
	  'status' => htmlspecialchars($this->input->post("Txtstatus", true))
    );
	$nik = $this->input->post('Txtnik');
	$sql = $this->db->query("SELECT nik FROM pemilih where nik='$nik'");
	$cek_nik = $sql->num_rows();
	if ($cek_nik > 0) {
	$this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4>Peringatan! Data Sudah Ada</h4></div>');
	redirect('pendukungfile');
	}else{
    $this->model_pemilih->simpan($data);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4>Success! Data Berhasil di Simpan<h4></div>');
    redirect('pendukungfile');
	}
  }

  public function edit($idjb)
  {
    $idjb = $this->uri->segment(3);

    $data = array(
      'title'     => 'Edit Pendukung',
      'data_jb' => $this->model_pemilih->edit($idjb),
	  'jenism' => ['pengurus partai', 'saksi', 'pl tps'],
	  'jns_tps' => $this->model_pemilih->get_tps(),
      'isi' => 'backend/pemilih/edit_pemilih'
    );

    $this->load->view('backend/layout/wrapper', $data);
  }

  public function update()
  {
    $id['nik'] = $this->input->post("Txtnik");
    $data = array(
      'nama_pemilih' => htmlspecialchars($this->input->post("Txtpemilih", true)),
	  'id_tps' => htmlspecialchars($this->input->post("Txttps", true)),
	  'kontak' => htmlspecialchars($this->input->post("Txtkontak", true)),
	  'status' => htmlspecialchars($this->input->post("Txtstatus", true))
    );

    $this->model_pemilih->update($data, $id);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! Data Berhasil di Update</div>');

    redirect('pendukungfile');
  }

  public function hapus($idj)
  {
    $id['nik'] = $this->uri->segment(3);
    $this->model_pemilih->hapus($id);
    redirect('pendukungfile');
  }
  
  public function uptodate(){
	$xkode			=htmlspecialchars($this->input->post("Txtnik", true));
	$nama_pendukung	=htmlspecialchars($this->input->post("Txtpemilih", true));
	$kontak			=htmlspecialchars($this->input->post("Txtkontak", true));
	$tps			=htmlspecialchars($this->input->post("Txttps", true));
	$status			=htmlspecialchars($this->input->post("Txtstatus", true));
	$this->model_pemilih->get_update($xkode,$nama_pendukung,$kontak,$tps,$status);
	redirect('pendukungfile');
  }
} // END OF class Pemilih