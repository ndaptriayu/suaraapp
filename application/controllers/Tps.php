<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tps extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('model_tps');
    $this->load->helper(array('form', 'url'));
	$this->load->library('form_validation');
        if ($this->session->userdata('status') != "loginCOD") {
            redirect(base_url("log-in"));
        }
  }

  public function index()
  {

    $this->load->view('index');
  }

  public function datatps()
  {

    $data = array(
      'title' => 'Data TPS',
      'data_tps' => $this->model_tps->get_all(),
      'isi' => 'backend/data_tps'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  public function tambah()
  {
    $data = array(
      'title' => 'Tambah TPS',
      'data_jb' => $this->model_tps->get_all(),
      'isi' => 'backend/tambah_tps'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  public function simpan()
  {
    $data = array(
      'nama_tps' => $this->input->post("Txttps")
    );
	
	$nama_tps = $this->input->post('Txttps');
	$sql = $this->db->query("SELECT nama_tps FROM tps where nama_tps='$nama_tps'");
	$cek_nik = $sql->num_rows();
	if ($cek_nik > 0) {
	$this->session->set_flashdata('notif', '<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Peringatan! Data TPS Sudah Ada</div>');
	redirect('tpsfile');
	}else{
    $this->model_tps->simpan($data);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! Data TPS Berhasil di Simpan</div>');

    redirect('tpsfile');
	}
  }

  public function hapus($idj)
  {
    $id['id_tps'] = $this->uri->segment(3);
    $this->model_tps->hapus($id);
    redirect('tpsfile');
  }
  
  public function update(){
	$xkode	=htmlspecialchars($this->input->post("id", true));
	$xnama	=htmlspecialchars($this->input->post("nama", true));
	$this->model_tps->get_update($xkode,$xnama);
	redirect('tpsfile');
  }

} // END OF class TPS
