<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengawas extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('model_kecamatan');
	$this->load->model('model_keldes');
    $this->load->model('model_tps');
	$this->load->model('model_dpt');
	$this->load->model('model_pemilih');
	$this->load->model('model_pengguna');
	$this->load->model('model_suara');
	$this->load->model('model_hitungan');
	$this->load->model('model_dapil');
	$this->load->helper(array('form', 'url'));
	$this->load->library('form_validation');
        if ($this->session->userdata('status') != "loginCOD") {
            redirect(base_url("log-in"));
        }
  }

  public function index()
  {
    $this->load->view('index');
  }

  public function kecamatan()
  {
    $data = array(
      'title' => 'Data Kecamatan',
      'data_kecamatan' => $this->model_kecamatan->get_all(),
      'isi' => 'backend/pengawas/pengawas_kecamatan'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }
  
  public function keldes()
  {
    $data = array(
      'title' => 'Data Kelurahan/Desa',
      'data_keldes' => $this->model_keldes->get_all(),
      'isi' => 'backend/pengawas/pengawas_keldes'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }
  
  public function tps()
  {
    $data = array(
      'title' => 'Data TPS',
      'data_tps' => $this->model_tps->get_all(),
      'isi' => 'backend/pengawas/pengawas_tps'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }
  
  public function dpt()
  {
    $data = array(
      'title' => 'Data DPT',
      'data_dpt' => $this->model_dpt->get_j(),
      'isi' => 'backend/pengawas/pengawas_dpt'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }
  
  public function pendukung()
  {
    $data = array(
      'title' => 'Data Pendukung',
      'data_pendukung' => $this->model_pemilih->get_j(),
	  'jns_tps' => $this->model_pemilih->get_tps(),
      'isi' => 'backend/pengawas/pengawas_pendukung'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }
  
  public function user()
  {
    $data = array(
      'title' => 'Data User',
      'data_user' => $this->model_pengguna->get_all(),
      'isi' => 'backend/pengawas/pengawas_user'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }
  
  public function suara()
  {
    $data = array(
      'title' => 'Data Suara',
      'data_suara' => $this->model_suara->get_j(),
      'isi' => 'backend/pengawas/pengawas_suara'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }
  
  // report suara
  public function total_suara()
  {
    $data = array(
      'title' => 'Total Perhitungan Suara',
      'data_suara' => $this->model_hitungan->get_j(),
	  'dropdown' => $this->model_hitungan->tampil_dropdown(),
	  'nik' => $this->model_hitungan->get_nik(),
	  'provinsi' => $this->model_hitungan->provinsi(),
	  'dtps' => $this->model_hitungan->get_tps(),
      'isi' => 'backend/pengawas/report/pengawas_totalsuara'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }
  public function rekap_realcount()
  {
    $data = array(
      'title' => 'Rekap Suara Kecamatan,Kel/Desa, TPS',
      'data_suara' => $this->model_hitungan->get_j(),
	  'data_perdapil' => $this->model_hitungan->get_d(),
	  'dropdowndapil' => $this->model_hitungan->tampil_dropdowndapil(),
	  'dropdown' => $this->model_hitungan->tampil_dropdown(),
	  'nik' => $this->model_hitungan->get_nik(),
	  'provinsi' => $this->model_hitungan->provinsi(),
	  'dtps' => $this->model_hitungan->get_tps(),
      'isi' => 'backend/pengawas/report/pengawas_rekaprealcount'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }
  public function rekap_dapil()
  {
    $data = array(
      'title' => 'Data Dapil',
      'pil' => $this->model_dapil->get_all(),
      'isi' => 'backend/pengawas/report/pengawas_rekapdapil'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }
  public function rekap_keldes()
  {
    $data = array(
      'title' => 'Rekap Suara Kecamatan, Kelurahan/Desa',
      'data_suara' => $this->model_hitungan->get_j(),
	  'dropdown' => $this->model_hitungan->tampil_dropdown(),
	  'nik' => $this->model_hitungan->get_nik(),
	  'provinsi' => $this->model_hitungan->provinsi(),
	  'dtps' => $this->model_hitungan->get_tps(),
      'isi' => 'backend/pengawas/report/pengawas_rekapkeldes'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }
} // END OF class kecamatan
