<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notify extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('model_notify');
    $this->load->helper(array('form', 'url'));
	$this->load->library('form_validation');
        if ($this->session->userdata('status') != "loginCOD") {
            redirect(base_url("log-in"));
        }
  }

  public function index()
  {

    $this->load->view('index');
  }

  public function datanotif()
  {
    $data = array(
	  'notif' => $this->model_notify->get_all(),
      'notif_desa' => $this->model_notify->tps_desa(),
	  'notif_dapil' => $this->model_notify->get_dapil(),
      'isi' => 'backend/data_notif'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

} // END OF class kecamatan
