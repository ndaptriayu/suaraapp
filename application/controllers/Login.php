<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('m_login');
    }

    function index()
    {
		$data = array(
		  'title' => 'SIPIL BUPATI 2020',
		  'Footer' => 'Karya Informatika'
		);
		$this->load->view('login', $data);
    }
	
	function dashboard()
    {
        $this->load->view('index');
    }
  
    function aksi_login()
    {
		$valid = $this->form_validation;
        $username = $this->input->post('user');
        $password = $this->input->post('sandi');
		$valid->set_rules('user', 'Username', 'required');
		$valid->set_rules('sandi', 'Password', 'required');

        $where = array(
            'username' => $username,
            'password' => md5($password)
        );
        $cek = $this->m_login->cek_login("tbl_adm", $where);
        if ($cek) {
            $data_session = array(
                'user_nama' => $username,
				'id_pengguna' => $cek['id_pengguna'],
                'status' => "loginCOD",
                'level' => $cek['level'],
                'id' => $cek['id_user'],
				'logged_in' => TRUE
            );
            $this->session->set_userdata($data_session);

            redirect(base_url("dashboard"));
        } else {
            $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible" role="alert">Username atau Password Anda Tidak Ditemukan</div>');
			
            redirect(base_url("login"));
        }
    }

    public function edit($idp)
    {
        $data = array(
            'title' => 'Edit Profil',
            'data_p' => $this->m_login->edit($idp),
            'isi' => 'backend/administrator/edit_profil'
        );
        $this->load->view('backend/layout/wrapper', $data);
    }

    public function update()
    {
        $id_u = $this->input->post("id");
        $password = $this->input->post("pass");
        $nama = $this->input->post("nama");
        $data = array(
            'nama' => $this->input->post("nama"),
            'username' => $this->input->post("username"),
            'password' => md5($password)
        );

        $this->m_login->update($data, $id_u);
        $this->session->set_userdata('nama', $nama);
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! data profil berhasil di update</div>');

        redirect(base_url("dashboard"));
    }
	
	
	
	function dataprofil($idp)
    {
        $data = array(
            'title' => 'Data Profil',
			'kecamatan' => $this->m_login->get_kecamatan(),
			'keldes' => $this->m_login->get_keldes(),
			'tps' => $this->m_login->get_tps(),
            'hasil' => $this->m_login->edit($idp),
			'profil' => $this->m_login->editx($idp),
            'isi' => 'backend/administrator/data_diri'
        );
        $this->load->view('backend/layout/wrapper', $data);
    }

    function tambah()
    {
        $data = array(
            'title' => 'Data Profil',
            'dataprofil' => $this->m_login->get_all(),
            'isi' => 'backend/administrator/tambah_profil'
        );
        $this->load->view('backend/layout/wrapper', $data);
    }
	
    public function hapus($id)
    {
        $idu['id_user'] = $this->uri->segment(3);

        $this->m_login->hapusu($idu);
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! data berhasil di hapus</div>');
        redirect('login/allprofil');
    }

    function logout()
    {
        $this->session->sess_destroy();
        redirect(base_url('login'));
    }


    // UNTUK SUPERADMIN
    function allprofil()
    {
        $data = array(
            'title' => 'Data Profil',
            'data_allprofil' => $this->m_login->get_all(),
            'isi' => 'backend/administrator/data_profilfull'
        );
        $this->load->view('backend/layout/wrapper', $data);
    }

    function tambah_p()
    {
        $data = array(
            'title' => 'Data Profil',
            'dataprofil' => $this->m_login->get_all(),
            'isi' => 'backend/administrator/tambah_profil'
        );
        $this->load->view('backend/layout/wrapper', $data);
    }

    public function simpan()
    {
        $data = array(
            'nama' => htmlspecialchars($this->input->post('nama', true)),
            'username' => htmlspecialchars($this->input->post('username', true)),
            'level' => 'admin',
            'password' => md5($this->input->post('pass'))
        );

        $this->m_login->simpan($data);
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! data berhasil di simpan</div>');

        redirect('login/allprofil');
    }

    public function update_p()
    {
        $id_u = $this->input->post("id");
        $password = $this->input->post("pass");
        $nama = $this->input->post("nama");
        $data = array(
            'nama' => $this->input->post("nama"),
            'username' => $this->input->post("username"),
            'password' => md5($password)

        );

        $this->m_login->update($data, $id_u);
        $this->session->set_userdata('nama', $nama);

        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! data profil berhasil di update</div>');

        redirect('login/allprofil');
    }
	
 }