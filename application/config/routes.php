<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'login/index';
$route['administrator'] = 'login/aksi_login';
$route['dashboard'] = 'admin/index';
$route['datadiri/(:num)'] = 'login/dataprofil/$1';
$route['logout'] = 'login/logout';
$route['notification'] = 'notify/datanotif';

$route['totalsuara'] = 'hitungan/total_hitungan';
$route['realcount'] = 'hitungan/datahitungan';
$route['dapilfile'] = 'dapil/datadapil';
$route['countkeldes'] = 'hitungan/rekapkeldes';
$route['countkecamatan'] = 'hitungan/rekapkecamatan';
$route['report_realcount'] = 'hitungan/report';
$route['report_dapil'] = 'dapil/cetak';
$route['report_kecamatan'] = 'kecamatan/cetak';
//star pengawas
$route['datakecamatan'] = 'pengawas/kecamatan';
$route['datakeldes'] = 'pengawas/keldes';
$route['datatps'] = 'pengawas/tps';

$route['datadpt'] = 'pengawas/dpt';
$route['datapendukung'] = 'pengawas/pendukung';
$route['datauser'] = 'pengawas/user';
$route['datasuara'] = 'pengawas/suara';

$route['perhitungansuara'] = 'pengawas/total_suara';
$route['rekapsuara'] = 'pengawas/rekap_realcount';
$route['rekapdapil'] = 'pengawas/rekap_dapil';
$route['rekapkeldes'] = 'pengawas/rekap_keldes';

//end pengawas

$route['dptfile'] = 'dpt/datadpt';
$route['userfile'] = 'pengguna/datapengguna';
$route['kecamatanfile'] = 'kecamatan/datakecamatan';
$route['keldesfile'] = 'keldes/datakeldes';
$route['tpsfile'] = 'tps/datatps';
$route['suarafile'] = 'suara/datasuara';
$route['pendukungfile'] = 'pemilih/datapemilih';
$route['api/totalsuara'] = 'api/getTotalSuara';
$route['api/totalsuarabydapil/(:any)'] = 'api/getTotalSuaraByDapil/$1';

$route['log-out'] = 'login/logout';
$route['log-in'] = 'login/index';
//$route['(.*)'] = "error404";
$route['translate_uri_dashes'] = FALSE;
