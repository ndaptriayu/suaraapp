<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- [ Main Content ] start -->
<section class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <h5 class="m-b-10"><?php echo $title ?></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <!-- [ Main Content ] start -->
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Filter Kecamatan</h5>
                    </div>
					
					<div class="card-body">						

						
						<form method="GET" action="">
							<?php $sid = $this->session->userdata['id_pengguna'];?>	
							<div class="row">
								<div class="col-sm-10">
									<div class="form-group">
										<select type="submit" name="cari" class="form-control" onchange="this.form.submit()">
											<option value='0'>--Pilih Kecamatan--</option>
											<?php
											$query = $this->db->query("SELECT nama_kecamatan AS kecamatan FROM kecamatan, keldes WHERE kecamatan.id_kecamatan = keldes.id_kecamatan GROUP BY nama_kecamatan ASC");
											foreach ($query->result_array() as $aa) {
											?>
											<option>
											<?php echo $aa['kecamatan'] ?>
											</option>
											<?php
											}
											?>
										</select>
									</div>
								</div>
								
								<div class="col-sm-2">
								<?php 
								if(isset($_GET['cari'])){
								$kecamatan=($_GET['cari']);
								$tg="report_kecamatan?kecamatan='$kecamatan'";
								?>
								<a style="padding: 0.35rem 1.1875rem;" class="btn btn-warning" href="<?php echo $tg ?>" target="_blank"><span class='glyphicon glyphicon-print'></span>  Excel</a>
								<?php
								}
								?>
								</div>
							  </div>
						</form>
					
					</div>
					
					
                </div>
            </div>
            <!-- [ form-element ] start -->
			<?php
			//di proses jika sudah klik tombol cari
			if(isset($_GET['cari'])){
				//menangkap nilai form
				$kecamatan=($_GET['cari']);
				$query=$this->db->query("
				SELECT
				Sum(suara.paslon4) as ps4,
				Sum(suara.paslon3) as ps3,
				Sum(suara.paslon2) as ps2,
				Sum(suara.paslon1) as ps1,
				Sum(suara.paslon1+suara.paslon2+suara.paslon3+suara.paslon4) as pstot,
				suara.id_kecamatan,
				suara.id_keldes,
				count(suara.id_tps) as tps,
				Sum(suara.total_dptb) as dptb,
				Sum(dpt.total_dpt) as dpt,
				Sum(suara.tidaksah) as taksah,
				kecamatan.nama_kecamatan,
				kecamatan.dapil,
				suara.id_adm,
				keldes.nama_keldes
				FROM suara 
				INNER JOIN kecamatan ON suara.id_kecamatan = kecamatan.id_kecamatan 
				INNER JOIN keldes ON suara.id_keldes = keldes.id_keldes 
				INNER JOIN tps ON suara.id_tps = tps.id_tps
				INNER JOIN dpt ON tps.id_tps = dpt.id_tps
				WHERE 
				kecamatan.nama_kecamatan like '%$kecamatan%'
				AND suara.id_kecamatan = dpt.id_kecamatan AND
				suara.id_keldes = dpt.id_keldes AND
				suara.id_tps = dpt.id_tps
				GROUP BY suara.id_keldes ASC");
			}else{
				$query=$this->db->query("
				SELECT
				Sum(suara.paslon4) as ps4,
				Sum(suara.paslon3) as ps3,
				Sum(suara.paslon2) as ps2,
				Sum(suara.paslon1) as ps1,
				Sum(suara.paslon1+suara.paslon2+suara.paslon3+suara.paslon4) as pstot,
				suara.id_kecamatan,
				suara.id_keldes,
				count(suara.id_tps) as tps,
				Sum(suara.total_dptb) as dptb,
				Sum(dpt.total_dpt) as dpt,
				Sum(suara.tidaksah) as taksah,
				kecamatan.nama_kecamatan,
				kecamatan.dapil,
				suara.id_adm,
				keldes.nama_keldes
				FROM
				suara 
				INNER JOIN kecamatan ON suara.id_kecamatan = kecamatan.id_kecamatan
				INNER JOIN keldes ON suara.id_keldes = keldes.id_keldes
				INNER JOIN tps ON suara.id_tps = tps.id_tps
				INNER JOIN dpt ON tps.id_tps = dpt.id_tps
				WHERE
				suara.id_kecamatan = dpt.id_kecamatan AND
				suara.id_keldes = dpt.id_keldes AND
				suara.id_tps = dpt.id_tps
				GROUP BY suara.id_keldes ASC");
			}
			?>		
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
						<table id="example" class="stripe hover text-center" style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
							<thead>
							<tr>
								<th>No.</th>
								<th>Kecamatan</th>
								<th>Kelurahan/Desa</th>
								<th>Jml tps</th>
								<th>Iin-Rahmad</th>
								<th>Junaidi-Sahrani</th>
								<th>Eryanto-Mateus</th>
								<th>Martin-Farhan</th>
								<th>Suara Sah</th>
								<th>Tidak Sah</th>
								<th>DPT+DPTB</th>
							</tr>
							</thead>
							<tbody>
								<?php
								//untuk penomoran data
								$no=1;							
								//menampilkan data
								foreach($query->result() as $hasil){
									$dpttotall= ($hasil->dpt)+($hasil->dptb);
								?>
							<tr>
								<td style="width: 8%;"><?php echo $no++ ?></td>
								<td><?php echo $hasil->nama_kecamatan ?></td>
								<td><?php echo $hasil->nama_keldes ?></td>
								<td><?php echo $hasil->tps ?></td>
								<td><?php echo $hasil->ps1 ?></td>
								<td><?php echo $hasil->ps2 ?></td>
								<td><?php echo $hasil->ps3 ?></td>
								<td><?php echo $hasil->ps4 ?></td>
								<td class="sah"><?php echo $hasil->pstot ?></td>
								<td class="tidaksah"><?php echo $hasil->taksah ?></td>
								<td class="paslon"><?php echo $dpttotall ?></td>
							</tr>
								<?php } ?>
							</tbody>
						</table>
                    </div>
                </div>
            </div>
			<?php
				unset($_GET['cari']);
			?>
        </div>
    </div>
</section>
