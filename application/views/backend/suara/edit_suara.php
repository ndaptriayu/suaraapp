<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<section class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
        <!-- [ Main Content ] start -->
			<div class="row">
				<div class="col-sm-12">
					<div class="card">
						<div class="card-header">
							<h5>Edit Data User</h5>
						</div>
						<div class="card-body">
						<?php echo $this->session->flashdata('notif') ?>                
						<?php echo form_open_multipart('suara/update') ?>
						<div class="row">
							<div class="col-sm-6">
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="hidden" name="Txtsuara" value="<?php echo $data_suara->id_suara ?>">
								<div class="form-group">
									<label for="text">TPS</label>
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
									<input type="number" name="Txttps" class="form-control" readonly value="<?php echo $data_suara->id_tps; ?>">
								</div>
								<div class="form-group">
									<label for="text">Suara Tidak Sah</label>
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
									<input type="number" name="Txttidaksah" class="form-control" placeholder="Masukkan Jumlah Suara Tidak Sah" required value="<?php echo $data_suara->tidaksah; ?>">
								</div>
								<div class="form-group">
									<label for="text">Suara DPTB</label>
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
									<input type="number" name="Txtdptb" class="form-control" placeholder="Masukkan Jumlah DPTB" required value="<?php echo $data_suara->total_dptb; ?>">
								</div>
								</div>
								<div class="col-sm-6">
								<div class="form-group">
									<label for="text">Suara Iin-Rahmad</label>
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
									<input type="number" name="Txtpaslon1" class="form-control" placeholder="Masukkan Jumlah Suara Sah Paslon 1" required value="<?php echo $data_suara->paslon1; ?>">
								</div>
								<div class="form-group">
									<label for="text">Suara Junaidi-Sahrani</label>
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
									<input type="number" name="Txtpaslon2" class="form-control" placeholder="Masukkan Jumlah Suara Sah Paslon 2" required value="<?php echo $data_suara->paslon2; ?>">
								</div>
								<div class="form-group">
									<label for="text">Suara Eryanto-Mateus</label>
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
									<input type="number" name="Txtpaslon3" class="form-control" placeholder="Masukkan Jumlah Suara Sah Paslon 3" required value="<?php echo $data_suara->paslon3; ?>">
								</div>
								<div class="form-group">
									<label for="text">Suara Martin-Farhan</label>
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
									<input type="number" name="Txtpaslon4" class="form-control" placeholder="Masukkan Jumlah Suara Sah Paslon 4" required value="<?php echo $data_suara->paslon4; ?>">
								</div>
								
							</div>
						</div>
							<button type="submit" class="btn btn-md btn-success">Update</button>
							<button type="button" class="btn btn-md btn-danger" onclick="javascript:history.back()"><span ></span> Batal</button>
							<?php echo form_close() ?>
						</div>
					</div>
				
				</div>
				<!-- [ form-element ] start -->
			</div>
		</div>
    </div>
</section>

<script type="text/javascript">
	$(document).ready(function(){		
		$('.form-checkbox').click(function(){
			if($(this).is(':checked')){
				$('.form-password').attr('type','text');
			}else{
				$('.form-password').attr('type','password');
			}
		});
	});
</script>