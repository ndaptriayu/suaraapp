<section class="pcoded-main-container">
    <div class="pcoded-content">
    <div class="page-header">
		<?php
			defined('BASEPATH') OR exit('No direct script access allowed');
			if ($this->session->userdata('level') == "superadmin") {
		?>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
					<div class="card-header">
                        <h5>Tambah Data Suara</h5>
                    </div>
					<div class="card-body">
					<div class="alert alert-mafan alert-dismissible" role="alert"><b><?php echo $this->session->flashdata('notif') ?>Data Hanya Bisa diinput 1 Kali, Kesalahan Data adalah Tanggung Jawab Penuh User Aplikasi.</b></div>

					<?php echo form_open_multipart('suara/simpan') ?>
					<?php $sid = $this->session->userdata['id_pengguna'];?>	
					  <div class="row needs-validation was-validated">
						<div class="col-sm-6">
							<div class="form-group">
							<input type="hidden" class="form-control" id="id_adm" name="id_adm" value="<?php echo $sid; ?>">
								<label for="text">Kelurahan/Desa</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<select class="custom-select"  name="Txtkeldes" id="perusahaan" required>
									<option value="" disabled selected>--Pilih--</option>
									<?php
										foreach ($dropdown->result() as $baris) {
										echo "<option value='".$baris->id_keldes."'>".$baris->nama_keldes."</option>";
										}
									?>
								</select>
							</div>							
							<div class="form-group">
								<label for="text">Kecamatan</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<select class="custom-select"  name="Txtkecamatan" id="cv" readonly required>
									<option value="" disabled selected>- Tampil Kecamatan -</option>
								</select>
							</div>						
							<div class="form-group">
								<label for="text">TPS</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<select class="form-control"  name="Txttps" id="tps" required>
									<option value="pilih" disabled selected>--Pilih--</option>
								</select>
							</div>
							<div class="form-group">
								<label for="text">Suara Tidak Sah</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="number" name="Txttidaksah" class="form-control" placeholder="Masukkan Jumlah Suara Tidak Sah" required value="<?= set_value('Txttidaksah'); ?>">
							</div>
							<div class="form-group">
								<label for="text">Suara DPTB</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="number" name="Txtdptb" class="form-control" placeholder="Masukkan Jumlah DPTB" required value="<?= set_value('Txtdptb'); ?>">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label for="text">Suara Iin-Rahmad</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="number" name="Txtpaslon1" class="form-control" placeholder="Masukkan Jumlah Suara Sah Paslon 1" required value="<?= set_value('Txtpaslon1'); ?>">
							</div>
							<div class="form-group">
								<label for="text">Suara Junaidi-Sahrani</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="number" name="Txtpaslon2" class="form-control" placeholder="Masukkan Jumlah Suara Sah Paslon 2" required value="<?= set_value('Txtpaslon2'); ?>">
							</div>
							<div class="form-group">
								<label for="text">Suara Eryanto-Mateus</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="number" name="Txtpaslon3" class="form-control" placeholder="Masukkan Jumlah Suara Sah Paslon 3" required value="<?= set_value('Txtpaslon3'); ?>">
							</div>
							<div class="form-group">
								<label for="text">Suara Martin-Farhan</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="number" name="Txtpaslon4" class="form-control" placeholder="Masukkan Jumlah Suara Sah Paslon 4" required value="<?= set_value('Txtpaslon4'); ?>">
							</div>
							
						</div>
					  </div>
						<button type="submit" class="btn btn-md btn-warning">Simpan</button>
						<?php echo form_close() ?>
					</div>
                </div>
            </div>
            <!-- [ form-element ] start -->
            <div class="col-sm-12">
                <div class="card">
					<div class="card-header">
                        <h5>Data Suara</h5>
                    </div>
                    <div class="card-body">
						<table id="example" class="stripe hover" style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
							<thead>
								<tr>
									<th>No.</th>
									<th>Kelurahan/Desa</th>
									<th>Kecamatan</th>									
									<th>Nama TPS</th>
									<th>Suara Paslon 1</th>
									<th>Suara Paslon 2</th>
									<th>Suara Paslon 3</th>
									<th>Suara Paslon 4</th>
									<th>Suara Tidak Sah</th>
									<th>Suara DPTB</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							<?php
							$no = 1;
							foreach ($data_suara as $hasil) {
							$xids=$hasil->id_suara;
							?>
								<tr>
									<td style="width: 6%;"><?php echo $no++ ?></td>
									<td><?php echo $hasil->nama_keldes ?></td>
									<td><?php echo $hasil->nama_kecamatan ?></td>									
									<td><?php echo $hasil->nama_tps ?></td>
									<td><?php echo $hasil->paslon1 ?></td>
									<td><?php echo $hasil->paslon2 ?></td>
									<td><?php echo $hasil->paslon3 ?></td>
									<td><?php echo $hasil->paslon4 ?></td>
									<td><?php echo $hasil->tidaksah ?></td>
									<td><?php echo $hasil->total_dptb ?></td>
									<td style="width: 20%;">
										<a href="<?php echo base_url() ?>suara/edit/<?php echo $xids ?>" class="btn btn-sm btn-success">Edit</a>
										<a href="<?php echo base_url() ?>suara/hapus/<?php echo $hasil->id_suara ?>" class="btn btn-sm btn-danger" onclick="return confirm('Yakin ingin menghapus ?')">Hapus</a>
									</td>
								</tr>
							<?php } ?>
							</tbody>
						</table>
                    </div>
                </div>
            </div>
        </div>
	<?php		
	}elseif($this->session->userdata('level') == "operasional") { 
	?>	
	<div class="row">
            <div class="col-sm-12">
                <div class="card">
				<div class="card-header">
					<h5>Tambah Data Suara</h5>
                </div>
					<div class="card-body">
					<div class="alert alert-mafan alert-dismissible" role="alert"><b><?php echo $this->session->flashdata('notif') ?>Data Hanya Bisa diinput 1 Kali, Kesalahan Data adalah Tanggung Jawab Penuh User Aplikasi.</b></div>

					<?php echo form_open_multipart('suara/simpan') ?>
					<?php $sid = $this->session->userdata['id_pengguna'];?>	
					  <div class="row needs-validation was-validated">
						<div class="col-sm-6">
							<div class="form-group">
							<input type="hidden" class="form-control" id="id_adm" name="id_adm" value="<?php echo $sid; ?>">
								<label for="text">Kelurahan/Desa</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<select class="custom-select"  name="Txtkeldes" id="perusahaan" required>
									<option value="" disabled selected>--Pilih--</option>
									<?php
										foreach ($dropdowndapil->result() as $baris) {
										if (($this->session->userdata['id_pengguna']) == ($baris->id_pengguna)) {
										echo "<option value='".$baris->id_keldes."'>".$baris->nama_keldes."</option>";
										}}
									?>
								</select>
							</div>							
							<div class="form-group">
								<label for="text">Kecamatan</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<select class="form-control"  name="Txtkecamatan" id="cv" readonly required>
									<option value="" disabled selected>- Tampil Kecamatan -</option>
								</select>
							</div>						
							<div class="form-group">
								<label for="text">TPS</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<select class="custom-select"  name="Txttps" id="tps" required>
									<option value="pilih" disabled selected>--Pilih--</option>
								</select>
							</div>
							<div class="form-group">
								<label for="text">Suara Tidak Sah</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="number" name="Txttidaksah" class="form-control" placeholder="Masukkan Jumlah Suara Tidak Sah" required value="<?= set_value('Txttidaksah'); ?>">
							</div>
							<div class="form-group">
								<label for="text">Suara DPTB</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="number" name="Txtdptb" class="form-control" placeholder="Masukkan Jumlah DPTB" required value="<?= set_value('Txtdptb'); ?>">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label for="text">Suara Iin-Rahmad</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="number" name="Txtpaslon1" class="form-control" placeholder="Masukkan Jumlah Suara Sah Paslon 1" required value="<?= set_value('Txtpaslon1'); ?>">
							</div>
							<div class="form-group">
								<label for="text">Suara Junaidi-Sahrani</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="number" name="Txtpaslon2" class="form-control" placeholder="Masukkan Jumlah Suara Sah Paslon 2" required value="<?= set_value('Txtpaslon2'); ?>">
							</div>
							<div class="form-group">
								<label for="text">Suara Eryanto-Mateus</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="number" name="Txtpaslon3" class="form-control" placeholder="Masukkan Jumlah Suara Sah Paslon 3" required value="<?= set_value('Txtpaslon3'); ?>">
							</div>
							<div class="form-group">
								<label for="text">Suara Martin-Farhan</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="number" name="Txtpaslon4" class="form-control" placeholder="Masukkan Jumlah Suara Sah Paslon 4" required value="<?= set_value('Txtpaslon4'); ?>">
							</div>
						</div>
					  </div>
						<button type="submit" class="btn btn-md btn-warning">Simpan</button>
						<?php echo form_close() ?>
					</div>
                </div>
            </div>
            <!-- [ form-element ] start -->
            <div class="col-sm-12">
                <div class="card">
				<div class="card-header">
					<h5>Data Suara</h5>
                </div>
                    <div class="card-body">
						<table id="example" class="stripe hover text-center" style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
							<thead>
								<tr>
									<th>No.</th>
									<th>Kelurahan/Desa</th>
									<th>Kecamatan</th>								
									<th>TPS</th>
									<th>ISO</th>
									<th>JUNAI</th>
									<th>ERAMAS</th>
									<th>MAFAN</th>
									<th>Tidak Sah</th>
									<th>Jml DPTB</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
							<?php
							$no = 1;
							foreach ($data_perdapil as $hasil) {
							if (($this->session->userdata['id_pengguna']) == ($hasil->id_pengguna)) {
							$xids=$hasil->id_suara;		
							?>
								<tr>
									<td style="width: 6%;"><?php echo $no++ ?></td>
									<td><?php echo $hasil->nama_keldes ?></td>
									<td><?php echo $hasil->nama_kecamatan ?></td>									
									<td><?php echo $hasil->nama_tps ?></td>									
									<td><?php echo $hasil->paslon1 ?></td>
									<td><?php echo $hasil->paslon2 ?></td>
									<td><?php echo $hasil->paslon3 ?></td>
									<td><?php echo $hasil->paslon4 ?></td>
									<td><?php echo $hasil->tidaksah ?></td>
									<td><?php echo $hasil->total_dptb ?></td>
									<td style="width: 20%;">
										<a href="<?php echo base_url() ?>suara/edit/<?php echo $xids ?>" class="btn btn-sm btn-success">Edit</a>
									</td>
								</tr>
							<?php }} ?>
							</tbody>
						</table>
                    </div>
                </div>
            </div>
        </div>
	<?php 
	}elseif($this->session->userdata('level') == "desa") { 
	?>
	<div class="row">
            <div class="col-sm-12">
                <div class="card">
					<div class="card-body">
					<div class="alert alert-mafan alert-dismissible" role="alert"><b><?php echo $this->session->flashdata('notif') ?>Data Hanya Bisa diinput 1 Kali, Kesalahan Data adalah Tanggung Jawab Penuh User Aplikasi.</b></div>
						<?php echo form_open_multipart('suara/simpan') ?>
						<?php $sid = $this->session->userdata['id_pengguna']; ?>
					  <div class="row needs-validation was-validated">
						<div class="col-sm-12">
							<div class="form-group">
							<input type="hidden" class="form-control" id="id_adm" name="id_adm" value="<?php echo $sid; ?>">
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="hidden" class="form-control"  name="Txtkeldes" value="<?php foreach ($nik as $jnis) { if (($this->session->userdata['id_pengguna']) == ($jnis->nik)) {echo $jnis->id_keldes; }} ?>" readonly>
							</div>							
							<div class="form-group">
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="hidden" class="form-control"  name="Txtkecamatan" value="<?php foreach ($nik as $jnis) { if (($this->session->userdata['id_pengguna']) == ($jnis->nik)) {echo $jnis->id_kecamatan; }} ?>" readonly>
							</div>						 
							<div class="form-group">
								<label for="text">TPS</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">

								<select class="custom-select"  name="Txttps" required>
									<option value="" disabled selected>--Pilih--</option>
									<?php
										foreach ($jns_tpsdesa->result() as $baris) {
										if (($this->session->userdata['id_pengguna']) == ($baris->id_pengguna)) {
										echo "<option value='".$baris->id_tps."'>".$baris->nama_tps."</option>";
										}}
									?>
								</select>
							</div>
							<div class="form-group">
								<label for="text">Suara Iin-Rahmad</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="number" name="Txtpaslon1" class="form-control" placeholder="Masukkan Jumlah Suara Sah Paslon 1" required value="<?= set_value('Txtpaslon1'); ?>">
							</div>
							<div class="form-group">
								<label for="text">Suara Junaidi-Sahrani</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="number" name="Txtpaslon2" class="form-control" placeholder="Masukkan Jumlah Suara Sah Paslon 2" required value="<?= set_value('Txtpaslon2'); ?>">
							</div>
							<div class="form-group">
								<label for="text">Suara Eryanto-Mateus</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="number" name="Txtpaslon3" class="form-control" placeholder="Masukkan Jumlah Suara Sah Paslon 3" required value="<?= set_value('Txtpaslon3'); ?>">
							</div>
							<div class="form-group">
								<label for="text">Suara Martin-Farhan</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="number" name="Txtpaslon4" class="form-control" placeholder="Masukkan Jumlah Suara Sah Paslon 4" required value="<?= set_value('Txtpaslon4'); ?>">
							</div>
							<div class="form-group">
								<label for="text">Suara Tidak Sah</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="number" name="Txttidaksah" class="form-control" placeholder="Masukkan Jumlah Suara Tidak Sah" required value="<?= set_value('Txttidaksah'); ?>">
							</div>
							<div class="form-group">
								<label for="text">Suara DPTB</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="number" name="Txtdptb" class="form-control" placeholder="Masukkan Jumlah DPTB" required value="<?= set_value('Txtdptb'); ?>">
							</div>
						</div>
					  </div>	
						<button type="submit" class="btn btn-md btn-warning">Simpan</button>
						<?php echo form_close() ?>
					</div>
                </div>
            </div>
            <!-- [ form-element ] start -->
        </div>
	<?php 
	}elseif($this->session->userdata('level') == "tps") { 
	?>
	<div class="row">
            <div class="col-sm-12">
                <div class="card">
					<div class="card-body">
					<div class="alert alert-mafan alert-dismissible" role="alert"><b><?php echo $this->session->flashdata('notif') ?>Data Hanya Bisa diinput 1 Kali, Kesalahan Data adalah Tanggung Jawab Penuh User Aplikasi.</b></div>
						<?php echo form_open_multipart('suara/simpan') ?>
						<?php $sid = $this->session->userdata['id_pengguna']; ?>
					  <div class="row needs-validation was-validated">
						<div class="col-sm-12">
							<div class="form-group">
							<input type="hidden" class="form-control" id="id_adm" name="id_adm" value="<?php echo $sid; ?>">
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="hidden" class="form-control"  name="Txtkeldes" value="<?php foreach ($nikpl as $jnis) { if (($this->session->userdata['id_pengguna']) == ($jnis->nik)) {echo $jnis->id_keldes; }} ?>" readonly>
							</div>							
							<div class="form-group">
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="hidden" class="form-control"  name="Txtkecamatan" value="<?php foreach ($nikpl as $jnis) { if (($this->session->userdata['id_pengguna']) == ($jnis->nik)) {echo $jnis->id_kecamatan; }} ?>" readonly>
							</div>						 
							<div class="form-group">
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="hidden" class="form-control"  name="Txttps" value="<?php foreach ($nikpl as $jnis) { if (($this->session->userdata['id_pengguna']) == ($jnis->nik)) {echo $jnis->id_tps; }} ?>" readonly>
							</div>
							<div class="form-group">
								<label for="text">Suara Iin-Rahmad</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="number" name="Txtpaslon1" class="form-control" placeholder="Masukkan Jumlah Suara Sah Paslon 1" required value="<?= set_value('Txtpaslon1'); ?>">
							</div>
							<div class="form-group">
								<label for="text">Suara Junaidi-Sahrani</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="number" name="Txtpaslon2" class="form-control" placeholder="Masukkan Jumlah Suara Sah Paslon 2" required value="<?= set_value('Txtpaslon2'); ?>">
							</div>
							<div class="form-group">
								<label for="text">Suara Eryanto-Mateus</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="number" name="Txtpaslon3" class="form-control" placeholder="Masukkan Jumlah Suara Sah Paslon 3" required value="<?= set_value('Txtpaslon3'); ?>">
							</div>
							<div class="form-group">
								<label for="text">Suara Martin-Farhan</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="number" name="Txtpaslon4" class="form-control" placeholder="Masukkan Jumlah Suara Sah Paslon 4" required value="<?= set_value('Txtpaslon4'); ?>">
							</div>
							<div class="form-group">
								<label for="text">Suara Tidak Sah</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="number" name="Txttidaksah" class="form-control" placeholder="Masukkan Jumlah Suara Tidak Sah" required value="<?= set_value('Txttidaksah'); ?>">
							</div>
							<div class="form-group">
								<label for="text">Suara DPTB</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="number" name="Txtdptb" class="form-control" placeholder="Masukkan Jumlah DPTB" required value="<?= set_value('Txtdptb'); ?>">
							</div>
						</div>
					  </div>	
						<button type="submit" class="btn btn-md btn-warning">Simpan</button>
						<?php echo form_close() ?>
					</div>
                </div>
            </div>
            <!-- [ form-element ] start -->
        </div>
	<?php
		}
	?>	
	</div>
    </div>
</section>
<script type="text/javascript">
	$(document).ready(function(){
		$('#perusahaan').on('change', function(){
			var id_perusahaan = $('#perusahaan').val();
			$.ajax({
			    type: 'POST',
			    url: '<?php echo base_url('Pemilih/tampil_chained') ?>',
			    data: { 'id' : id_perusahaan },
				success: function(data){
				    $("#cv").html(data);
				}
			})
		})
		$('#perusahaan').on('change', function(){
			var id_perusahaan = $('#perusahaan').val();
			$.ajax({
			    type: 'POST',
			    url: '<?php echo base_url('Pemilih/tampil_chained2') ?>',
			    data: { 'id' : id_perusahaan },
				success: function(data){
				    $("#tps").html(data);
				}
			})
		})
	})
</script>