<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
			<!-- footer area start-->
			<footer>
				<div class="card-body text-center">
					<h5 style="color:#fff;margin-bottom: 0px;">© Copyright 2020. KARYA INFORMATIKA.</h5>
				</div>
			</footer>
			<!-- footer area end-->
		</div>

		<script src="<?php echo base_url() ?>assets/js/vendor-all.min.js"></script>
		<script src="<?php echo base_url() ?>assets/js/plugins/bootstrap.min.js"></script>
		<script src="<?php echo base_url() ?>assets/js/pcoded.min.js"></script>
		<script>
			$('#exampleModal').on('show.bs.modal', function(event) {
				var button = $(event.relatedTarget)
				var recipient = button.data('whatever')
				var modal = $(this)
				modal.find('.modal-title').text('New message to ' + recipient)
				modal.find('.modal-body input').val(recipient)
			})
		</script>

		<script src="<?php echo base_url() ?>assets/table/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url() ?>assets/table/dataTables.responsive.min.js"></script>
		<script	src="<?php echo base_url() ?>assets/table/script.js"></script>
		
		<script src="<?php echo base_url() ?>assets/select2/select2.min.js"></script>
		<script src="<?php echo base_url() ?>assets/select2/popper.min.js"></script>
		<script	src="<?php echo base_url() ?>assets/select2/script.js"></script>
	</body>
</html>
