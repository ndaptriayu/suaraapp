<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!doctype html>
<html lang="en">
<head>
    <title>SIPIL BUPATI KETAPANG 2020</title>
    <!-- HTML5 Shim and Respond.js IE11 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 11]>
    	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    	<![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="Karya Informatika" />
    <meta name="keywords" content="Karya Informatika">
    <meta name="author" content="Phoenixcoded" />
    <!-- Favicon icon -->

    <link href="<?php echo base_url() ?>assets/images/avatar.png" type="image/x-icon" rel="icon" >
	<link href="<?php echo base_url() ?>assets/table/tailwind.min.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>assets/table/jquery.dataTables.min.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>assets/table/responsive.dataTables.min.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>assets/table/style.css" rel="stylesheet" >
    <!-- vendor css style="text-transform: capitalize;" -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/style.css">
	
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/select2/select2.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/select2/select2-bootstrap4.min.css">
	<!-- js" -->
	<script src="<?php echo base_url() ?>web/jquery-3.5.1.min.js"></script>
	<script src="<?php echo base_url() ?>web/jquery.basictable.min.js"></script>
	
</head>
<style>
table {
	text-transform: uppercase;
}

.paslon{
font-weight:bolder;
color: var(--gray);
}
.sah{
font-weight:bolder;
color: #FF9800;
}
.tidaksah{
font-weight:bolder;	
color:#9C27B0;
}
.total{
font-weight:bolder;	
color:var(--cyan);
}

body {
    font-family: "Robot", sans-serif;
}

.alert-mafan {
    color: #000000;
    background-color: #fbec1c;
    border-color: #ffecd0;
}
</style>
<body style="background-image: url('<?php echo base_url() ?>gambar/Splash.png');">

	<!-- [ Header ] start -->
	<header class="navbar pcoded-header navbar-expand-lg navbar-light header-blue">
				<div class="m-header">
					<a class="mobile-menu" id="mobile-collapse" href="#!"><span></span></a>
					<a href="#!" class="b-brand">
						<!-- ========   change your logo hear   ============ -->
						<h4 style="margin-bottom: 0px;color: #fff;font-size: 1.3rem;">PILKADA 2020</h4>
					</a>
					<a href="#!" class="mob-toggler">
						<i class="feather icon-more-vertical"></i>
					</a>
				</div>

				<div class="collapse navbar-collapse">
				<label style="margin-bottom: 0px;" class="btn-sm btn-secondary btn-rounded"><?php echo $this->session->userdata("user_nama"); ?></label>
				</div>
	</header>
	<!-- [ Header ] end -->