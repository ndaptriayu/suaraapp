<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>	
<!-- [ Main Content ] start -->
<section class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <h5 class="m-b-10"><?php echo $title ?></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <!-- [ Main Content ] start -->
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Tambah Data Kecamatan</h5>
                    </div>
					<div class="card-body">
					<?php echo $this->session->flashdata('notif') ?>
						<?php echo form_open_multipart('kecamatan/simpan') ?>
						<div class="form-group">
							<label for="text">Nama kecamatan</label>
							<input type="text" name="Txtkecamatan" class="form-control" placeholder="Masukkan Nama Kecamatan" required autofocus>
						</div>

						<button type="submit" class="btn btn-md btn-warning">Simpan</button>

						<?php echo form_close() ?>
					</div>
                </div>
			
            </div>
            <!-- [ form-element ] start -->
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <h5>Data Kecamatan</h5>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
								<div class="container w-full md:w-4/5 xl:w-3/5  mx-auto px-2">
									 <div id='recipients' class="p-8 mt-6 lg:mt-0 rounded shadow bg-white">
										<table id="example" class="stripe hover text-center" style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
											<thead>
												<tr>
													<th>No.</th>
													<th>Nama Kecamatan</th>
													<th>Options</th>
												</tr>
											</thead>
											<tbody>
											<?php
											$no = 1;
											foreach ($data_kecamatan as $hasil) {
											$xids=$hasil->id_kecamatan;
											?>
												<tr>
													<td style="width: 8%;"><?php echo $no++ ?></td>
													<td><?php echo $hasil->nama_kecamatan ?></td>
													<td style="width: 20%;">
														<a href="#" class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal_edit<?php echo $xids;?>"> Edit</a>
													</td>
												</tr>
											<?php } ?>
											</tbody>
										</table>
									</div>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ============ MODAL EDIT BARANG =============== -->
<?php 
	foreach ($data_kecamatan as $hasil) {
	$xids=$hasil->id_kecamatan;
	$nama=$hasil->nama_kecamatan;
?>
<!-- [ vertically-modal ] start -->
	<div id="modal_edit<?php echo $xids;?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<?php echo form_open_multipart('kecamatan/update') ?>
                <div class="modal-body">
					<div class="card-body">
						<div class="row needs-validation was-validated">
						<div class="col-sm-12">
							<div class="form-group">
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="hidden" name="id" value="<?php echo $xids; ?>">
							</div>						
							<div class="form-group">
								<label for="text">Nama Kecamatan</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="text" name="nama" class="form-control" required value="<?php echo $nama; ?>">
							</div>
						</div>
						</div>
						<button class="btn btn-md btn-success">Update</button>
						<button class="btn btn-md btn-danger" data-dismiss="modal" aria-hidden="true">Tutup</button>
					</div>	
                </div>
				<?php echo form_close() ?>
			</div>
		</div>
	</div>
<?php } ?>
    <!--END MODAL ADD BARANG-->