<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	if ($this->session->userdata('level') == "superadmin") {
?>
<!-- [ Main Content ] start -->
<section class="pcoded-main-container">
<div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
	<div class="page-header">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Data Saksi, PL TPS, Pengurus Partai</h5>
                    </div>
					<div class="card-body">
					<div class="alert alert-mafan alert-dismissible" role="alert"><b><?php echo $this->session->flashdata('notif') ?>Data Hanya Bisa diinput 1 Kali, Kesalahan Data adalah Tanggung Jawab Penuh User Aplikasi.</b></div>
					
						<?php echo form_open_multipart('pemilih/simpan') ?>
						<?php $sid = $this->session->userdata['user_nama'];?>
						<div class="row needs-validation was-validated">
						<div class="col-sm-6">
							<div class="form-group">
								<label for="text">NIK</label>
								<input type="hidden" class="form-control" id="id_adm" name="id_adm" value="<?php echo $sid; ?>">
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="number" maxlength="16" name="Txtnik" class="form-control" placeholder="Masukkan NIK Pendukung" required autofocus value="<?= set_value('Txtnik'); ?>">
							</div>
							
							<div class="form-group">
								<label for="text">Nama Pendukung</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="text" name="Txtpemilih" class="form-control" placeholder="Masukkan Nama Pendukung" required value="<?= set_value('Txtpemilih'); ?>">
							</div>
							
							<div class="form-group">
								<label for="text">Kontak Pendukung</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="number" name="Txtkontak" class="form-control" placeholder="Masukkan Nomor HP Aktif" required value="<?= set_value('Txtkontak'); ?>">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label for="text">Kelurahan/Desa</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<select class="custom-select"  name="Txtkeldes" id="perusahaan" required>
									<option value="" disabled selected>--Pilih--</option>
									<?php
										foreach ($dropdown->result() as $baris) {
										echo "<option value='".$baris->id_keldes."'>".$baris->nama_keldes." ".$baris->nama_dapil."</option>";
										}
									?>
								</select>
							</div>
							<div class="form-group">
								<label for="text">Kecamatan</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >
								<select class="form-control"  name="Txtkecamatan" id="cv" readonly>
									<option value="pilih" disabled selected>- Tampil Kecamatan -</option>
								</select>
							</div>
							
							<div class="form-group">
								<label for="text">Status</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >
								<select class="custom-select"  name="Txtstatus" required>
									<option value="" disabled selected>--Pilih--</option>
									<option value="pengurus partai">Pengurus Partai</option>
									<option value="saksi">Saksi</option>
									<option value="pl tps">PL TPS</option>
								</select>
							</div>
							<div class="form-group">
								<label for="text">TPS (Tidak Perlu Dipilih Jika Bukan "Saksi")</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<select class="custom-select"  name="Txttps">
									<option value="" disabled selected>--Pilih--</option>
									<?php
									foreach ($jns_tps as $Dttps) {
										?>
										<option value="<?= $Dttps->id_tps ?>"><?= $Dttps->nama_tps ?></option>
									<?php
									}
									?>
								</select>
							</div>
									
						</div>
						</div>
						<button type="submit" class="btn btn-md btn-warning">Simpan</button>
						<?php echo form_close() ?>
					</div>
                </div>
            </div>
            <!-- [ form-element ] start -->
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
						<table id="example" class="stripe hover text-center" style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
								<thead>
									<tr>
										<th>No.</th>
										<th>NIK</th>
										<th>Nama Pendukung</th>
										<th>Kecamatan</th>
										<th>Kelurahan/Desa</th>
										<th>TPS</th>
										<th>Kontak</th>
										<th>Status</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
								<?php
								$no = 1;
								foreach ($data_pemilih as $hasil) {
								$xids=$hasil->nik;
								?>
									<tr>
										<td style="width: 8%;"><?php echo $no++ ?></td>
										<td><?php echo $hasil->nik ?></td>
										<td><?php echo $hasil->nama_pemilih ?></td>
										<td><?php echo $hasil->nama_kecamatan ?></td>
										<td><?php echo $hasil->nama_keldes ?></td>
										<td><?php foreach ($jns_tps as $stat) {
											if (($hasil->id_tps) == ($stat->id_tps)) {
											 echo $stat->nama_tps; }
										}?></td>
										<td><?php echo $hasil->kontak ?></td>
										<td><?php echo $hasil->status ?></td>
										
										<td style="width: 20%;">

											<a href="<?php echo base_url() ?>pemilih/edit/<?php echo $xids ?>" class="btn btn-sm btn-success">Edit</a>
											<a href="<?php echo base_url() ?>pemilih/hapus/<?php echo $hasil->nik ?>" class="btn btn-sm btn-danger" onclick="return confirm('Yakin ingin menghapus <?php echo $hasil->nama_pemilih ?> ?')">Hapus</a>
										</td>
										
									</tr>
								<?php } ?>
								</tbody>
						</table>
                    </div>
                </div>
            </div>
        </div>
	</div>
</div>
</section>
<?php		
	}elseif($this->session->userdata('level') == "operasional") { 
?>
<!-- [ Main Content ] start -->
<section class="pcoded-main-container">
<div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
	<div class="page-header">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Data Saksi, PL TPS, Pengurus Partai</h5>
                    </div>
					<div class="card-body">
					<div class="alert alert-mafan alert-dismissible" role="alert"><b><?php echo $this->session->flashdata('notif') ?>Data Hanya Bisa diinput 1 Kali, Kesalahan Data adalah Tanggung Jawab Penuh User Aplikasi.</b></div>
					
						<?php echo form_open_multipart('pemilih/simpan') ?>
						<?php $sid = $this->session->userdata['user_nama'];?>
						<div class="row needs-validation was-validated">
						<div class="col-sm-6">
							<div class="form-group">
								<label for="text">NIK</label>
								<input type="hidden" class="form-control" id="id_adm" name="id_adm" value="<?php echo $sid; ?>">
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="number" maxlength="16" name="Txtnik" class="form-control" placeholder="Masukkan NIK Pendukung" required autofocus value="<?= set_value('Txtnik'); ?>">
							</div>
							
							<div class="form-group">
								<label for="text">Nama Pendukung</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="text" name="Txtpemilih" class="form-control" placeholder="Masukkan Nama Pendukung" required value="<?= set_value('Txtpemilih'); ?>">
							</div>
							
							<div class="form-group">
								<label for="text">Kontak Pendukung</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="number" name="Txtkontak" class="form-control" placeholder="Masukkan Nomor HP Aktif" required value="<?= set_value('Txtkontak'); ?>">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label for="text">Kelurahan/Desa</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<select class="custom-select"  name="Txtkeldes" id="perusahaan" required>
									<option value="" disabled selected>--Pilih--</option>
									<?php
										foreach ($dropdowndapil->result() as $baris) {
										if (($this->session->userdata['id_pengguna']) == ($baris->id_pengguna)) {
										echo "<option value='".$baris->id_keldes."'>".$baris->nama_keldes."</option>";
										}}
									?>
								</select>
							</div>
							<div class="form-group">
								<label for="text">Kecamatan</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >
								<select class="form-control"  name="Txtkecamatan" id="cv" readonly>
									<option value="pilih" disabled selected>- Tampil Kecamatan -</option>
								</select>
							</div>
							
							<div class="form-group">
								<label for="text">Status</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >
								<select class="custom-select"  name="Txtstatus" required>
									<option value="" disabled selected>--Pilih--</option>
									<option value="pengurus partai">Pengurus Partai</option>
									<option value="saksi">Saksi</option>
									<option value="pl tps">PL TPS</option>
								</select>
							</div>
							<div class="form-group">
								<label for="text">TPS (Tidak Perlu Dipilih Jika Bukan "Saksi")</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<select class="custom-select"  name="Txttps">
									<option value="" disabled selected>--Pilih--</option>
									<?php
									foreach ($jns_tps as $Dttps) {
										?>
										<option value="<?= $Dttps->id_tps ?>"><?= $Dttps->nama_tps ?></option>
									<?php
									}
									?>
								</select>
							</div>
									
						</div>
						</div>
						<button type="submit" class="btn btn-md btn-warning">Simpan</button>
						<?php echo form_close() ?>
					</div>
                </div>
            </div>
            <!-- [ form-element ] start -->
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
						<table id="example" class="stripe hover text-center" style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
								<thead>
									<tr>
										<th>No.</th>
										<th>NIK</th>
										<th>Nama Pendukung</th>
										<th>Kecamatan</th>
										<th>Kelurahan/Desa</th>
										<th>TPS</th>
										<th>Kontak</th>
										<th>Status</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
								<?php
								$no = 1;
								foreach ($data_perdapil as $hasil) {
								$xids=$hasil->nik;
								if (($this->session->userdata['id_pengguna']) == ($hasil->id_pengguna)) {
									?>
									<tr>
										<td style="width: 8%;"><?php echo $no++ ?></td>
										<td><?php echo $hasil->nik ?></td>
										<td><?php echo $hasil->nama_pemilih ?></td>
										<td><?php echo $hasil->nama_kecamatan ?></td>
										<td><?php echo $hasil->nama_keldes ?></td>
										<td><?php foreach ($jns_tps as $stat) {
											if (($hasil->id_tps) == ($stat->id_tps)) {
											 echo $stat->nama_tps; }
										}?></td>
										<td><?php echo $hasil->kontak ?></td>
										<td><?php echo $hasil->status ?></td>
										
										<td style="width: 20%;">
											<a href="<?php echo base_url() ?>pemilih/edit/<?php echo $xids ?>" class="btn btn-sm btn-success">Edit</a>
											<a href="<?php echo base_url() ?>pemilih/hapus/<?php echo $hasil->nik ?>" class="btn btn-sm btn-danger" onclick="return confirm('Yakin ingin menghapus <?php echo $hasil->nama_pemilih ?> ?')">Hapus</a>
										</td>
										
									</tr>
								<?php }} ?>
								</tbody>
						</table>
                    </div>
                </div>
            </div>
        </div>
	</div>
</div>
</section>
<?php
	}
?>


<script type="text/javascript">
	$(document).ready(function(){
		$('#perusahaan').on('change', function(){
			var id_perusahaan = $('#perusahaan').val();
			$.ajax({
			    type: 'POST',
			    url: '<?php echo base_url('Pemilih/tampil_chained') ?>',
			    data: { 'id' : id_perusahaan },
				success: function(data){
				    $("#cv").html(data);
				}
			})
		})
	})
</script>