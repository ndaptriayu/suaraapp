<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	if ($this->session->userdata('level') == "superadmin") {
	}elseif($this->session->userdata('level') == "operasional") {
?>	
<!-- [ Main Content ] start -->
<section class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <h5 class="m-b-10"><?php echo $title ?></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <!-- [ Main Content ] start -->
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Profil Admin</h5>
                    </div>
					<div class="card-body">
					<input type="hidden" name="id" value="<?php echo $hasil->id_user ?>">
						<div class="form-group">
							<label for="text">Username</label>
							<input type="text" name="Txthp" class="form-control" value="<?php echo $hasil->username ?>" readonly>
						</div>
						<div class="form-group">
							<label for="text">Level</label>
							<input type="text" name="Txthp" class="form-control" value="<?php echo $hasil->level ?>" readonly>
						</div>
						<div class="form-group">
							<label for="text">DAPIL</label>
							<input type="text" name="Txtnamapengguna" class="form-control" value="<?php
								foreach ($kecamatan as $jnis) {
                                if (($profil->id_kecamatan) == ($jnis->id_kecamatan)) {
									echo $jnis->dapil;}
                                }
                            ?>" readonly>
						</div>
						<button type="button" class="btn btn-md btn-danger" onclick="javascript:history.back()"><span ></span> Batal</button>
					</div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php 
	}elseif($this->session->userdata('level') == "desa") { 
?>
<!-- [ Main Content ] start -->
<section class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <h5 class="m-b-10"><?php echo $title ?></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <!-- [ Main Content ] start -->
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Profil Admin</h5>
                    </div>
					<div class="card-body">
					<input type="hidden" name="id" value="<?php echo $hasil->id_user ?>">
						<div class="form-group">
							<label for="text">Nama User Akun</label>
							<input type="text" name="Txthp" class="form-control" value="<?php echo $profil->nama_pemilih ?>" readonly>
						</div>
						<div class="form-group">
							<label for="text">Kecamatan</label>
							<input type="text" name="Txtnamapengguna" class="form-control" value="<?php
								foreach ($kecamatan as $jnis) {
                                if (($profil->id_kecamatan) == ($jnis->id_kecamatan)) {
									echo $jnis->nama_kecamatan;}
                                }
                            ?>" readonly>
						</div>
						<div class="form-group">
							<label for="text">Kelurahan/Desa</label>
							<input type="text" name="Txtnamapengguna" class="form-control" value="<?php
								foreach ($keldes as $jnis) {
                                if (($profil->id_keldes) == ($jnis->id_keldes)) {
									echo $jnis->nama_keldes;}
                                }
                            ?>" readonly>							
						</div>						
						<button type="button" class="btn btn-md btn-danger" onclick="javascript:history.back()"><span ></span> Batal</button>
					</div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php 
	}elseif($this->session->userdata('level') == "tps") { 
?>
<!-- [ Main Content ] start -->
<section class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <h5 class="m-b-10"><?php echo $title ?></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <!-- [ Main Content ] start -->
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Profil Admin</h5>
                    </div>
					<div class="card-body">
					<input type="hidden" name="id" value="<?php echo $hasil->id_user ?>">
						<div class="form-group">
							<label for="text">Nama User Akun</label>
							<input type="text" name="Txthp" class="form-control" value="<?php echo $profil->nama_pemilih ?>" readonly>
						</div>
						<div class="form-group">
							<label for="text">Kecamatan</label>
							<input type="text" name="Txtnamapengguna" class="form-control" value="<?php
								foreach ($kecamatan as $jnis) {
                                if (($profil->id_kecamatan) == ($jnis->id_kecamatan)) {
									echo $jnis->nama_kecamatan;}
                                }
                            ?>" readonly>
						</div>
						<div class="form-group">
							<label for="text">Kelurahan/Desa</label>
							<input type="text" name="Txtnamapengguna" class="form-control" value="<?php
								foreach ($keldes as $jnis) {
                                if (($profil->id_keldes) == ($jnis->id_keldes)) {
									echo $jnis->nama_keldes;}
                                }
                            ?>" readonly>						
						</div>
						<div class="form-group">
							<label for="text">TPS</label>
							<input type="text" name="Txtnamapengguna" class="form-control" value="<?php
								foreach ($tps as $jnis) {
                                if (($profil->id_tps) == ($jnis->id_tps)) {
									echo $jnis->nama_tps;}
                                }
                            ?>" readonly>						
						</div>						
						<button type="button" class="btn btn-md btn-danger" onclick="javascript:history.back()"><span ></span> Batal</button>
					</div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php }?>