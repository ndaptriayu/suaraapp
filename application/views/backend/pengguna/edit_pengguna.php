<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	if ($this->session->userdata('level') == "superadmin") {
?>
<section class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
        <!-- [ Main Content ] start -->
			<div class="row">
				<div class="col-sm-12">
					<div class="card">
						<div class="card-header">
							<h5>Edit Data User</h5>
						</div>
						<div class="card-body">
						<?php echo $this->session->flashdata('notif') ?>                
						<?php echo form_open_multipart('pengguna/update') ?>
							<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="hidden" name="idu" value="<?php echo $data_u->id_user ?>">						
									<label for="text">NIK</label>
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
									<select class="form-control"  name="nik">
									<option disabled selected>--Pilih--</option>
										<?php
										foreach ($jns_nik as $Dtnik) {
											?>
											<option value="<?= $Dtnik->nik ?>" <?php if (($data_u->nik) == ($Dtnik->nik)) {	echo 'selected'; } ?> ><?= $Dtnik->nik ?> <?= $Dtnik->nama_pemilih ?></option>
										<?php
										}
										?>
									</select>
								</div>								
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="hidden" name="status" class="form-control" value="<?php echo $data_u->level ?>">
								<div class="form-group">
									<label for="text">Username</label>
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
									<input type="text" name="username" class="form-control" value="<?php echo $data_u->username ?>">
								</div>
								<div class="form-group">
									<label for="text">Password Baru</label>
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
									<input type="password" name="pass" class="form-control form-password" placeholder="Masukkan Password Baru" value="<?= set_value('pass'); ?>" required>
									<input type="checkbox" class="form-checkbox"> Show password
								</div>										
							</div>
							</div>
							<button type="submit" class="btn btn-md btn-success">Update</button>
							<button type="button" class="btn btn-md btn-danger" onclick="javascript:history.back()"><span ></span> Batal</button>
							<?php echo form_close() ?>
						</div>
					</div>
				
				</div>
				<!-- [ form-element ] start -->
			</div>
		</div>
    </div>
</section>
<?php		
	}elseif($this->session->userdata('level') == "operasional") { 
?>
<section class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
        <!-- [ Main Content ] start -->
			<div class="row">
				<div class="col-sm-12">
					<div class="card">
						<div class="card-header">
							<h5>Edit Data User</h5>
						</div>
						<div class="card-body">
						<?php echo $this->session->flashdata('notif') ?>                
						<?php echo form_open_multipart('pengguna/update') ?>
							<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="hidden" name="idu" value="<?php echo $data_u->id_user ?>">						
									<label for="text">NIK</label>
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
									<select class="form-control"  name="nik">
									<option disabled selected>--Pilih--</option>
										<?php
										foreach ($jns_nik as $Dtnik) {
											?>
											<option value="<?= $Dtnik->nik ?>" <?php if (($data_u->nik) == ($Dtnik->nik)) {	echo 'selected'; } ?> ><?= $Dtnik->nik ?> <?= $Dtnik->nama_pemilih ?></option>
										<?php
										}
										?>
									</select>
								</div>
								<div class="form-group">
									<label for="text">Level</label>
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
									<select class="mb-3 form-control"  name="status" id="exampleFormControlSelect1" required>
									<option disabled selected>--Pilih--</option>
									<?php
									  foreach ($jenism as $jnis) {
									?>
										<option value="<?= $jnis; ?>" <?php if (($data_u->level) == ($jnis)) {	echo 'selected'; } ?> >
											<?= $jnis; ?>
										</option>
									<?php
									  }
									?>
									</select>
								</div>
								<div class="form-group">
									<label for="text">Username</label>
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
									<input type="text" name="username" class="form-control" value="<?php echo $data_u->username ?>">
								</div>
								<div class="form-group">
									<label for="text">Password Baru</label>
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
									<input type="password" name="pass" class="form-control form-password" placeholder="Masukkan Password Baru" value="<?= set_value('pass'); ?>" required>
									<input type="checkbox" class="form-checkbox"> Show password
								</div>										
							</div>
							</div>
							<button type="submit" class="btn btn-md btn-success">Update</button>
							<button type="button" class="btn btn-md btn-danger" onclick="javascript:history.back()"><span ></span> Batal</button>
							<?php echo form_close() ?>
						</div>
					</div>
				
				</div>
				<!-- [ form-element ] start -->
			</div>
		</div>
    </div>
</section>
<?php
	}
?>
<script type="text/javascript">
	$(document).ready(function(){		
		$('.form-checkbox').click(function(){
			if($(this).is(':checked')){
				$('.form-password').attr('type','text');
			}else{
				$('.form-password').attr('type','password');
			}
		});
	});
</script>