<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header("Content-type: application/octet-stream");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table border=1 class="text-center" style="width:100%; ">
	<thead>
		<tr>
			<th>No.</th>
			<th>Nama Kecamatan</th>
			<th>Iin-Rahmad</th>
			<th>Junaidi-Sahrani</th>
			<th>Eryanto-Mateus</th>
			<th>Martin-Farhan</th>
			<th>Suara Sah</th>
			<th>Tidak Sah</th>
		</tr>
	</thead>
	<tbody>
	<?php

		$dapil=($_GET['dapil']);
		$dadapil=$this->db->query('SELECT 
			Sum(suara.paslon4) as ps4,
			Sum(suara.paslon3) as ps3,
			Sum(suara.paslon2) as ps2,
			Sum(suara.paslon1) as ps1,
			Sum(suara.paslon1+suara.paslon2+suara.paslon3+suara.paslon4) as pstot,
			suara.id_kecamatan,
			Sum(suara.tidaksah) as taksah,
			kecamatan.nama_kecamatan,
			kecamatan.dapil
			FROM suara INNER JOIN kecamatan ON suara.id_kecamatan=kecamatan.id_kecamatan
			WHERE kecamatan.dapil='.$dapil.'
			GROUP BY
			suara.id_kecamatan');
			header("Content-Disposition: attachment; filename=$dapil.xls");

	$no = 1;
	foreach($dadapil ->result_array() as $u){
	?>
		<tr>
			<td style="width: 8%;"><?php echo $no++ ?></td>
			<td><?php echo $u['nama_kecamatan'] ?></td>
			<td><?php echo $u['ps1'] ?></td>
			<td><?php echo $u['ps2'] ?></td>
			<td><?php echo $u['ps3'] ?></td>
			<td><?php echo $u['ps4'] ?></td>
			<td class="sah"><?php echo $u['pstot'] ?></td>
			<td class="tidaksah"><?php echo $u['taksah'] ?></td>
		</tr>
	<?php } ?>
	</tbody>
</table>