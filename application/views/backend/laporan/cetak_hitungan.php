<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header("Content-type: application/octet-stream");
header("Pragma: no-cache");
header("Expires: 0");
?>

	<?php
		//di proses jika sudah klik tombol cari
		if(isset($_POST['cari'])){
			//menangkap nilai form
			$kecamatan=($_POST['kecamatan']);
			$keldes=($_POST['keldes']);
			$query=$this->db->query("SELECT			suara.id_adm,suara.paslon1,suara.paslon2,suara.paslon3,suara.paslon4,suara.tidaksah,suara.total_dptb,dpt.total_dpt,kecamatan.nama_kecamatan,keldes.nama_keldes,tps.nama_tps
			FROM suara
			INNER JOIN kecamatan ON suara.id_kecamatan = kecamatan.id_kecamatan
			INNER JOIN keldes ON suara.id_keldes = keldes.id_keldes
			INNER JOIN tps ON suara.id_tps = tps.id_tps
			INNER JOIN dpt ON tps.id_tps = dpt.id_tps 
			WHERE 
			kecamatan.nama_kecamatan like '%$kecamatan%' AND keldes.id_keldes like '%$keldes%'
			AND suara.id_kecamatan = dpt.id_kecamatan AND
			suara.id_keldes = dpt.id_keldes AND
			suara.id_tps = dpt.id_tps
			order by suara.id_tps ASC");
		}else{
			$query=$this->db->query("SELECT suara.id_adm,suara.paslon1,suara.paslon2,suara.paslon3,suara.paslon4,suara.tidaksah,suara.total_dptb,dpt.total_dpt,kecamatan.nama_kecamatan,keldes.nama_keldes,tps.nama_tps
			FROM suara
			INNER JOIN kecamatan ON suara.id_kecamatan = kecamatan.id_kecamatan
			INNER JOIN keldes ON suara.id_keldes = keldes.id_keldes
			INNER JOIN tps ON suara.id_tps = tps.id_tps
			INNER JOIN dpt ON tps.id_tps = dpt.id_tps
			WHERE
			suara.id_kecamatan = dpt.id_kecamatan AND
			suara.id_keldes = dpt.id_keldes AND
			suara.id_tps = dpt.id_tps
			order by suara.id_kecamatan ASC");
		}
	?>
<table width="100%" border="0">
  <tr>
    <td width="14%">NAMA KECAMATAN</td>
    <td width="76%">: <?php echo $kecamatan ?></td>
  </tr>
  <tr>
    <td>NAMA KELURAHAN/DESA</td>
	<?php 
	$ee=$this->db->query("select * from keldes INNER JOIN kecamatan ON keldes.id_kecamatan = kecamatan.id_kecamatan where keldes.id_keldes like '%$keldes%' AND kecamatan.nama_kecamatan like '%$kecamatan%'")->result_array();
	foreach($ee as $z){
	?>
	<td>
		: <?php echo $z['nama_keldes'];?>
	</td>
	<?php } ?>	
  </tr>
</table>
<BR />
<table border=1 width="100%" style="text-align:center;">
	<thead>
	<tr>
		<th>NO. TPS</th>
		<th>ISO</th>
		<th>JUNAI</th>
		<th>ERAMAS</th>
		<th>MAFAN</th>
		<th>JML SUARA SAH</th>
		<th>JML TIDAK SAH</th>
		<th>JML DPT</th>
		<th>JML DPTB</th>
		<th>JML GOLPUT</th>
	</tr>
	</thead>
	<tbody>
	<?php						
	//menampilkan data
	foreach($query->result() as $hasil){
	$title=$hasil->nama_keldes;
	$total= ($hasil->paslon1)+($hasil->paslon2)+($hasil->paslon3)+($hasil->paslon4);
	$dpttotall= ($hasil->total_dpt)+($hasil->total_dptb);
	$golput=$dpttotall-($total+$hasil->tidaksah);
	header("Content-Disposition: attachment; filename=$title.xls");
	?>
	<tr>
		<td><?php echo $hasil->nama_tps ?></td>
		<td><?php echo $hasil->paslon1 ?></td>
		<td><?php echo $hasil->paslon2 ?></td>
		<td><?php echo $hasil->paslon3 ?></td>
		<td><?php echo $hasil->paslon4 ?></td>
		<td class="sah"><?php echo $total ?></td>
		<td class="tidaksah"><?php echo $hasil->tidaksah ?></td>
		<td class="paslon"><?php echo $hasil->total_dpt ?></td>
		<td class="paslon"><?php echo $hasil->total_dptb ?></td>
		<td class="paslon"><?php echo $golput ?></td>
	</tr>
	<?php } ?>
	
	<?php
			$query=$this->db->query("SELECT
			Sum(suara.paslon1+suara.paslon2+suara.paslon3+suara.paslon4) AS pstot,
			Sum(suara.paslon1) AS ps1,
			Sum(suara.paslon2) AS ps2,
			Sum(suara.paslon3) AS ps3,
			Sum(suara.paslon4) AS ps4,
			Sum(suara.tidaksah) AS tidaksah,
			Sum(suara.total_dptb) AS dptb,
			Sum(dpt.total_dpt) AS dpt,
			Sum(suara.total_dptb+dpt.total_dpt) AS dptot,
			Count(suara.id_tps) AS tpstot
			FROM suara
			INNER JOIN kecamatan ON suara.id_kecamatan = kecamatan.id_kecamatan
			INNER JOIN keldes ON suara.id_keldes = keldes.id_keldes
			INNER JOIN tps ON suara.id_tps = tps.id_tps
			INNER JOIN dpt ON tps.id_tps = dpt.id_tps 
			WHERE 
			kecamatan.nama_kecamatan like '%$kecamatan%' AND keldes.id_keldes like '%$keldes%'
			AND suara.id_kecamatan = dpt.id_kecamatan AND
			suara.id_keldes = dpt.id_keldes AND
			suara.id_tps = dpt.id_tps
			ORDER BY
			kecamatan.nama_kecamatan ASC,
			keldes.nama_keldes ASC,
			tps.nama_tps ASC");
			foreach($query->result() as $hasil){
			$alldpt=$hasil->dptot;
			$allsuara=$hasil->pstot+$hasil->tidaksah;
			$allgolput=$alldpt-$allsuara;
	?>
	<tr style="font-weight:900;">
		<td><?php echo $hasil->tpstot ?></td>
		<td><?php echo number_format($hasil->ps1, 0, '.', '.'); ?></td>
		<td><?php echo number_format($hasil->ps2, 0, '.', '.'); ?></td>
		<td><?php echo number_format($hasil->ps3, 0, '.', '.'); ?></td>
		<td><?php echo number_format($hasil->ps4, 0, '.', '.'); ?></td>
		<td><?php echo number_format($hasil->pstot, 0, '.', '.'); ?></td>
		<td><?php echo number_format($hasil->tidaksah, 0, '.', '.'); ?></td>
		<td><?php echo number_format($hasil->dpt, 0, '.', '.'); ?></td>
		<td><?php echo number_format($hasil->dptb, 0, '.', '.'); ?></td>
		<td><?php echo number_format($allgolput) ?></td>
	</tr>
	<?php } ?>
	</tbody>
</table>
