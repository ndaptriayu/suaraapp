<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>	
<!-- [ Main Content ] start -->
<section class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <h5 class="m-b-10"><?php echo $title ?></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <!-- [ Main Content ] start -->
        <div class="row">

            <!-- [ form-element ] start -->
            <div class="col-sm-12">
                <div class="card">
					<div class="card-header">
                        <h5>Data Suara</h5>
                    </div>
                    <div class="card-body">
						<table id="example" class="stripe hover" style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
							<thead>
								<tr>
									<th>No.</th>
									<th>Kelurahan/Desa</th>
									<th>Kecamatan</th>									
									<th>Nama TPS</th>
									<th>Suara Paslon 1</th>
									<th>Suara Paslon 2</th>
									<th>Suara Paslon 3</th>
									<th>Suara Paslon 4</th>
									<th>Suara Tidak Sah</th>
									<th>Suara DPTB</th>
								</tr>
							</thead>
							<tbody>
							<?php
							$no = 1;
							foreach ($data_suara as $hasil) {
							$xids=$hasil->id_suara;
							?>
								<tr>
									<td style="width: 8%;"><?php echo $no++ ?></td>
									<td><?php echo $hasil->nama_keldes ?></td>
									<td><?php echo $hasil->nama_kecamatan ?></td>									
									<td><?php echo $hasil->nama_tps ?></td>
									<td><?php echo $hasil->paslon1 ?></td>
									<td><?php echo $hasil->paslon2 ?></td>
									<td><?php echo $hasil->paslon3 ?></td>
									<td><?php echo $hasil->paslon4 ?></td>
									<td><?php echo $hasil->tidaksah ?></td>
									<td><?php echo $hasil->total_dptb ?></td>
								</tr>
							<?php } ?>
							</tbody>
						</table>
                    </div>
                </div>
            </div>
        </div>
	</div>
</section>