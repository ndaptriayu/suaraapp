<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- [ Main Content ] start -->
<section class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <h5 class="m-b-10"><?php echo $title ?></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <!-- [ Main Content ] start -->
        <div class="row">

            <!-- [ form-element ] start -->
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <h5>Data TPS</h5>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
								<div class="container w-full md:w-4/5 xl:w-3/5  mx-auto px-2">
									 <div id='recipients' class="p-8 mt-6 lg:mt-0 rounded shadow bg-white">
										<table id="example" class="stripe hover text-center" style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
											<thead>
												<tr>
													<th>No.</th>
													<th>Nama TPS</th>
												</tr>
											</thead>
											<tbody>
											<?php
											$no = 1;
											foreach ($data_tps as $hasil) {
											?>
												<tr>
													<td style="width: 8%;"><?php echo $no++ ?></td>
													<td><?php echo $hasil->nama_tps ?></td>
												</tr>
											<?php } ?>
											</tbody>
										</table>
									</div>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>