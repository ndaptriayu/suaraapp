<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>	
<!-- [ Main Content ] start -->
<section class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <h5 class="m-b-10"><?php echo $title ?></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <!-- [ Main Content ] start -->
        <div class="row">

            <!-- [ form-element ] start -->
            <div class="col-sm-12">
                <div class="card">
					<div class="card-header">
                        <h5>Data DPT</h5>
                    </div>
                    <div class="card-body">
						<table id="example" class="stripe hover" style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
							<thead>
								<tr>
									<th>No.</th>
									<th>Kecamatan</th>
									<th>Kelurahan/Desa</th>
									<th>Nama TPS</th>
									<th>Total DPT</th>
								</tr>
							</thead>
							<tbody>
							<?php
							$no = 1;
							foreach ($data_dpt as $hasil) {
								$xids=$hasil->id_dpt;
								?>
								<tr>
									<td style="width: 8%;"><?php echo $no++ ?></td>
									<td><?php echo $hasil->nama_kecamatan ?></td>
									<td><?php echo $hasil->nama_keldes ?></td>
									<td><?php echo $hasil->nama_tps ?></td>
									<td><?php echo $hasil->total_dpt ?></td>
								</tr>
							<?php } ?>
							</tbody>
						</table>
                    </div>
                </div>
            </div>
        </div>
	</div>
</section>