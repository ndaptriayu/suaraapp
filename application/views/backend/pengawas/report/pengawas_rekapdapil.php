<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>	
<!-- [ Main Content ] start -->
<section class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <h5 class="m-b-10"><?php echo $title ?></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <!-- [ Main Content ] start -->
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Tampil Data Dapil</h5>
                    </div>
					<div class="card-body">						
						<form action="" method="get">
							<div class="form-group">
								<select type="submit" name="tt" class="form-control" onchange="this.form.submit()">
									<option disabled selected>--Pilih--</option>
									<?php 
										foreach($pil as $p){
									?>
									<option>
									<?php echo $p->dapil ?>
									</option>
									<?php
										}
									?>
								</select>
							</div>
						</form>
					</div>
                </div>			
            </div>
            <!-- [ form-element ] start -->
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <?php 
							if(isset($_GET['tt'])){
								echo "<h5><a style='color:orange'> ". $_GET['tt']."</a></h5>";
							}
						?>
                        <hr>
						<table id="example" class="stripe hover text-center" style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
							<thead>
								<tr>
									<th>No.</th>
									<th>Nama Kecamatan</th>
									<th>Iin-Rahmad</th>
									<th>Junaidi-Sahrani</th>
									<th>Eryanto-Mateus</th>
									<th>Martin-Farhan</th>
									<th>Suara Sah</th>
									<th>Tidak Sah</th>
								</tr>
							</thead>
							<tbody>
							<?php
							if(isset($_GET['tt'])){
								$dapil=($_GET['tt']);
								$dadapil=$this->db->query("SELECT 
									Sum(suara.paslon4) as ps4,
									Sum(suara.paslon3) as ps3,
									Sum(suara.paslon2) as ps2,
									Sum(suara.paslon1) as ps1,
									Sum(suara.paslon1+suara.paslon2+suara.paslon3+suara.paslon4) as pstot,
									suara.id_kecamatan,
									suara.tidaksah,
									kecamatan.nama_kecamatan,
									kecamatan.dapil
									FROM suara INNER JOIN kecamatan ON suara.id_kecamatan=kecamatan.id_kecamatan
									WHERE kecamatan.dapil LIKE '$dapil'
									GROUP BY
									suara.id_kecamatan
								");

							}else{
								$dadapil=$this->db->query("SELECT
									Sum(suara.paslon4) as ps4,
									Sum(suara.paslon3) as ps3,
									Sum(suara.paslon2) as ps2,
									Sum(suara.paslon1) as ps1,
									Sum(suara.paslon1+suara.paslon2+suara.paslon3+suara.paslon4) as pstot,
									suara.id_kecamatan,
									suara.tidaksah,
									kecamatan.nama_kecamatan,
									kecamatan.dapil
									FROM suara INNER JOIN kecamatan ON suara.id_kecamatan=kecamatan.id_kecamatan
									GROUP BY
									suara.id_kecamatan
								");
							}
							$no = 1;
							foreach($dadapil ->result_array() as $u){
							?>
								<tr>
									<td style="width: 8%;"><?php echo $no++ ?></td>
									<td><?php echo $u['nama_kecamatan'] ?></td>
									<td><?php echo $u['ps1'] ?></td>
									<td><?php echo $u['ps2'] ?></td>
									<td><?php echo $u['ps3'] ?></td>
									<td><?php echo $u['ps4'] ?></td>
									<td class="sah"><?php echo $u['pstot'] ?></td>
									<td class="tidaksah"><?php echo $u['tidaksah'] ?></td>
								</tr>
							<?php } ?>
							</tbody>
						</table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>