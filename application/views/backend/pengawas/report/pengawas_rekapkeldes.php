<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- [ Main Content ] start -->
<section class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <h5 class="m-b-10"><?php echo $title ?></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <!-- [ Main Content ] start -->
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Filter Kelurahan/Desa</h5>
                    </div>
					<div class="card-body">
					<form method="POST" action="" class="text-center">
					<?php $sid = $this->session->userdata['id_pengguna'];?>	
					  <div class="row">
						<div class="col-sm-5">
							<div class="form-group">
							<input type="hidden" class="form-control" id="id_adm" name="id_adm" value="<?php echo $sid; ?>">
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<select class="custom-select"  name="keldes" id="perusahaan" required>
									<option value="" disabled selected>--Pilih Kelurahan/Desa--</option>
									<?php
										foreach ($dropdown->result() as $baris) {
										echo "<option value='".$baris->id_keldes."'>".$baris->nama_keldes."</option>";
										}
									?>
								</select>
							</div>
						</div>	
						<div class="col-sm-5">
							<div class="form-group">
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<select class="custom-select"  name="kecamatan" id="cv" readonly required>
									<option value="" disabled selected>- Tampil Kecamatan -</option>
								</select>
							</div>
						</div>	
						<div class="col-sm-2">
						<button style="padding: 0.35rem 1.1875rem;" type="submit" name="cari" class="btn btn-warning  has-ripple" value="Filter">Filter</button>
						</div>
					  </div>
					</form>
					</div>
                </div>
            </div>
            <!-- [ form-element ] start -->
			<?php
			//di proses jika sudah klik tombol cari
			if(isset($_POST['cari'])){
				//menangkap nilai form
				$kecamatan=($_POST['kecamatan']);
				$keldes=($_POST['keldes']);
				$query=$this->db->query("
				SELECT
				Sum(suara.paslon4) as ps4,
				Sum(suara.paslon3) as ps3,
				Sum(suara.paslon2) as ps2,
				Sum(suara.paslon1) as ps1,
				Sum(suara.paslon1+suara.paslon2+suara.paslon3+suara.paslon4) as pstot,
				suara.id_kecamatan,
				suara.id_keldes,
				suara.id_tps,
				suara.tidaksah,
				kecamatan.nama_kecamatan,
				kecamatan.dapil,
				suara.id_adm,
				keldes.nama_keldes
				FROM suara 
				INNER JOIN kecamatan ON suara.id_kecamatan = kecamatan.id_kecamatan 
				INNER JOIN keldes ON suara.id_keldes = keldes.id_keldes 
				INNER JOIN tps ON suara.id_tps = tps.id_tps
				INNER JOIN dpt ON tps.id_tps = dpt.id_tps
				WHERE 
				kecamatan.nama_kecamatan like '%$kecamatan%' AND keldes.id_keldes like '%$keldes%'
				AND suara.id_kecamatan = dpt.id_kecamatan AND
				suara.id_keldes = dpt.id_keldes AND
				suara.id_tps = dpt.id_tps
				GROUP BY suara.id_keldes ASC");
			}else{
				$query=$this->db->query("
				SELECT
				Sum(suara.paslon4) as ps4,
				Sum(suara.paslon3) as ps3,
				Sum(suara.paslon2) as ps2,
				Sum(suara.paslon1) as ps1,
				Sum(suara.paslon1+suara.paslon2+suara.paslon3+suara.paslon4) as pstot,
				suara.id_kecamatan,
				suara.id_keldes,
				suara.id_tps,
				suara.tidaksah,
				kecamatan.nama_kecamatan,
				kecamatan.dapil,
				suara.id_adm,
				keldes.nama_keldes
				FROM
				suara 
				INNER JOIN kecamatan ON suara.id_kecamatan = kecamatan.id_kecamatan
				INNER JOIN keldes ON suara.id_keldes = keldes.id_keldes
				INNER JOIN tps ON suara.id_tps = tps.id_tps
				INNER JOIN dpt ON tps.id_tps = dpt.id_tps
				WHERE
				suara.id_kecamatan = dpt.id_kecamatan AND
				suara.id_keldes = dpt.id_keldes AND
				suara.id_tps = dpt.id_tps
				GROUP BY suara.id_keldes ASC");
			}
			?>		
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
						<table id="example" class="stripe hover text-center" style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
							<thead>
							<tr>
								<th>No.</th>
								<th>Kecamatan</th>
								<th>Kelurahan/tps</th>
								<th>Iin-Rahmad</th>
								<th>Junaidi-Sahrani</th>
								<th>Eryanto-Mateus</th>
								<th>Martin-Farhan</th>
								<th>Suara Sah</th>
								<th>Tidak Sah</th>
							</tr>
							</thead>
							<tbody>
								<?php
								//untuk penomoran data
								$no=1;							
								//menampilkan data
								foreach($query->result() as $hasil){
								?>
							<tr>
								<td style="width: 8%;"><?php echo $no++ ?></td>
								<td><?php echo $hasil->nama_kecamatan ?></td>
								<td><?php echo $hasil->nama_keldes ?></td>
								<td><?php echo $hasil->ps1 ?></td>
								<td><?php echo $hasil->ps2 ?></td>
								<td><?php echo $hasil->ps3 ?></td>
								<td><?php echo $hasil->ps4 ?></td>
								<td class="sah"><?php echo $hasil->pstot ?></td>
								<td class="tidaksah"><?php echo $hasil->tidaksah ?></td>
							</tr>
								<?php } ?>
							</tbody>
						</table>
                    </div>
                </div>
            </div>
			<?php
				unset($_POST['cari']);
			?>
        </div>
    </div>
</section>

<script type="text/javascript">
	$(document).ready(function(){
		$('#perusahaan').on('change', function(){
			var id_perusahaan = $('#perusahaan').val();
			$.ajax({
			    type: 'POST',
			    url: '<?php echo base_url('Hitungan/tampil_chained') ?>',
			    data: { 'id' : id_perusahaan },
				success: function(data){
				    $("#cv").html(data);
				}
			})
		})
	})
</script>