<section class="pcoded-main-container">
    <div class="pcoded-content">
    <div class="page-header">
		<?php
			defined('BASEPATH') OR exit('No direct script access allowed');
			if ($this->session->userdata('level') == "superadmin") {
		?>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
					<div class="card-header">
                        <h5>Tambah Data DPT</h5>
                    </div>
					<div class="card-body">
					<div class="alert alert-mafan alert-dismissible" role="alert"><b><?php echo $this->session->flashdata('notif') ?>Data Hanya Bisa diinput 1 Kali, Kesalahan Data adalah Tanggung Jawab Penuh User Aplikasi.</b></div>

					<?php echo form_open_multipart('dpt/simpan') ?>
					<?php $sid = $this->session->userdata['id_pengguna'];?>	
					  <div class="row needs-validation was-validated">
						<div class="col-sm-12">
							<div class="form-group">
							<input type="hidden" class="form-control" id="id_adm" name="id_adm" value="<?php echo $sid; ?>">
								<label for="text">Kelurahan/Desa</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<select class="custom-select"  name="Txtkeldes" id="perusahaan" required>
									<option value="" disabled selected>--Pilih--</option>
									<?php
										foreach ($dropdown->result() as $baris) {
										echo "<option value='".$baris->id_keldes."'>".$baris->nama_keldes."</option>";
										}
									?>
								</select>
							</div>
							<div class="form-group">
								<label for="text">Kecamatan</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<select class="custom-select"  name="Txtkecamatan" id="cv" readonly required>
									<option value="" disabled selected>- Tampil Kecamatan -</option>
								</select>
							</div>
							<div class="form-group">
								<label for="text">TPS</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<select class="custom-select"  name="Txttps" required>
									<option value="" disabled selected>--Pilih--</option>
									<?php
									foreach ($jns_tps as $Dttps) {
										?>
										<option value="<?= $Dttps->id_tps ?>"><?= $Dttps->nama_tps ?></option>
									<?php
									}
									?>
								</select>
							</div>
							<div class="form-group">
								<label for="text">Total DPT</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="number" name="Txtdpt" class="form-control" placeholder="Masukkan Jumlah Suara Tidak Sah" required value="<?= set_value('Txtdpt'); ?>">
							</div>
						</div>
					  </div>
						<button type="submit" class="btn btn-md btn-warning">Simpan</button>
						<?php echo form_close() ?>
					</div>
                </div>
            </div>
            <!-- [ form-element ] start -->
            <div class="col-sm-12">
                <div class="card">
					<div class="card-header">
                        <h5>Data DPT</h5>
                    </div>
                    <div class="card-body">
						<table id="example" class="stripe hover" style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
							<thead>
								<tr>
									<th>No.</th>
									<th>Kecamatan</th>
									<th>Kelurahan/Desa</th>
									<th>Nama TPS</th>
									<th>Total DPT</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							<?php
							$no = 1;
							foreach ($data_dpt as $hasil) {
								$xids=$hasil->id_dpt;
								?>
								<tr>
									<td style="width: 8%;"><?php echo $no++ ?></td>
									<td><?php echo $hasil->nama_kecamatan ?></td>
									<td><?php echo $hasil->nama_keldes ?></td>
									<td><?php echo $hasil->nama_tps ?></td>
									<td><?php echo $hasil->total_dpt ?></td>
									<td style="width: 20%;">
										<a href="#" class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal_edit<?php echo $xids;?>"> Edit</a>
										<a href="<?php echo base_url() ?>dpt/hapus/<?php echo $hasil->id_dpt ?>" class="btn btn-sm btn-danger">Hapus</a>
									</td>
								</tr>
							<?php } ?>
							</tbody>
						</table>
                    </div>
                </div>
            </div>
        </div>
	<?php		
	}elseif($this->session->userdata('level') == "operasional") { 
	?>	
	<div class="row">
            <div class="col-sm-12">
                <div class="card">
				<div class="card-header">
					<h5>Tambah Data DPT</h5>
                </div>
					<div class="card-body">
					<div class="alert alert-mafan alert-dismissible" role="alert"><b><?php echo $this->session->flashdata('notif') ?>Data Hanya Bisa diinput 1 Kali, Kesalahan Data adalah Tanggung Jawab Penuh User Aplikasi.</b></div>

					<?php echo form_open_multipart('dpt/simpan') ?>
					<?php $sid = $this->session->userdata['id_pengguna'];?>	
					  <div class="row needs-validation was-validated">
						<div class="col-sm-12">
							<div class="form-group">
							<input type="hidden" class="form-control" id="id_adm" name="id_adm" value="<?php echo $sid; ?>">
								<label for="text">Kelurahan/Desa</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<select class="custom-select"  name="Txtkeldes" id="perusahaan" required>
									<option value="" disabled selected>--Pilih--</option>
									<?php
										foreach ($dropdowndapil->result() as $baris) {
										if (($this->session->userdata['id_pengguna']) == ($baris->id_pengguna)) {
										echo "<option value='".$baris->id_keldes."'>".$baris->nama_keldes."</option>";
										}}
									?>
								</select>
							</div>
							<div class="form-group">
								<label for="text">Kecamatan</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<select class="form-control"  name="Txtkecamatan" id="cv" readonly required>
									<option value="" disabled selected>- Tampil Kecamatan -</option>
								</select>
							</div>
							<div class="form-group">
								<label for="text">TPS</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<select class="custom-select"  name="Txttps" required>
									<option value="" disabled selected>--Pilih--</option>
									<?php
									foreach ($jns_tps as $Dttps) {
										?>
										<option value="<?= $Dttps->id_tps ?>"><?= $Dttps->nama_tps ?></option>
									<?php
									}
									?>
								</select>
							</div>
							<div class="form-group">
								<label for="text">Total DPT</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="number" name="Txtdpt" class="form-control" placeholder="Masukkan Jumlah Suara Tidak Sah" required value="<?= set_value('Txtdpt'); ?>">
							</div>
						</div>
					  </div>
						<button type="submit" class="btn btn-md btn-warning">Simpan</button>
						<?php echo form_close() ?>
					</div>
                </div>
            </div>
            <!-- [ form-element ] start -->
            <div class="col-sm-12">
                <div class="card">
				<div class="card-header">
					<h5>Data DPT</h5>
                </div>
                    <div class="card-body">
						<table id="example" class="stripe hover" style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
							<thead>
								<tr>
									<th>No.</th>
									<th>Kecamatan</th>
									<th>Kelurahan/Desa</th>
									<th>Nama TPS</th>
									<th>Total DPT</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							<?php
							$no = 1;
							foreach ($data_dpt as $hasil) {
								if (($this->session->userdata['id_pengguna']) == ($hasil->id_adm)) {
									$xids=$hasil->id_dpt;
								?>
								<tr>
									<td style="width: 8%;"><?php echo $no++ ?></td>
									<td><?php echo $hasil->nama_kecamatan ?></td>
									<td><?php echo $hasil->nama_keldes ?></td>
									<td><?php echo $hasil->nama_tps ?></td>
									<td><?php echo $hasil->total_dpt ?></td>
									<td style="width: 20%;">
										<a href="#" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#modal_edit<?php echo $xids;?>"> Edit</a>
										<a href="<?php echo base_url() ?>dpt/hapus/<?php echo $hasil->id_dpt ?>" class="btn btn-sm btn-danger">Hapus</a>
									</td>
								</tr>
							<?php }} ?>
							</tbody>
						</table>
                    </div>
                </div>
            </div>
        </div>
	<?php
		}
	?>	
	</div>
    </div>
</section>
<!-- ============ MODAL EDIT BARANG =============== -->
<?php 
	foreach ($data_dpt as $hasil) {
	$xids=$hasil->id_dpt;
	$nama=$hasil->total_dpt;
	$kecamatan=$hasil->nama_kecamatan;
	$keldes=$hasil->nama_keldes;
	$tps=$hasil->nama_tps;
?>
<!-- [ vertically-modal ] start -->
	<div id="modal_edit<?php echo $xids;?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<?php echo form_open_multipart('dpt/update') ?>
                <div class="modal-body">
					<div class="card-body">
						<div class="row needs-validation was-validated">
						<div class="col-sm-12">
							<div class="form-group">
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="hidden" name="id" value="<?php echo $xids; ?>">
							</div>
							<div class="form-group">
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="text" name="nama" class="form-control" readonly value="<?php echo $kecamatan; ?>">

								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="text" name="nama" class="form-control" readonly value="<?php echo $keldes; ?>">

								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="text" name="nama" class="form-control" readonly value="<?php echo $tps; ?>">
							</div>
							<div class="form-group">
								<label for="text">Total DPT</label>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<input type="text" name="nama" class="form-control" required value="<?php echo $nama; ?>">
							</div>
						</div>
						</div>
						<button class="btn btn-md btn-success">Update</button>
						<button class="btn btn-md btn-danger" data-dismiss="modal" aria-hidden="true">Tutup</button>
					</div>	
                </div>
				<?php echo form_close() ?>
			</div>
		</div>
	</div>
<?php } ?>
    <!--END MODAL ADD BARANG-->
<script type="text/javascript">
	$(document).ready(function(){
		$('#perusahaan').on('change', function(){
			var id_perusahaan = $('#perusahaan').val();
			$.ajax({
			    type: 'POST',
			    url: '<?php echo base_url('Pemilih/tampil_chained') ?>',
			    data: { 'id' : id_perusahaan },
				success: function(data){
				    $("#cv").html(data);
				}
			})
		})
		$('#perusahaan').on('change', function(){
			var id_perusahaan = $('#perusahaan').val();
			$.ajax({
			    type: 'POST',
			    url: '<?php echo base_url('Pemilih/tampil_chained2') ?>',
			    data: { 'id' : id_perusahaan },
				success: function(data){
				    $("#tps").html(data);
				}
			})
		})
	})
</script>