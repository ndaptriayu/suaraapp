<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style>
@media only screen and (max-width: 575px){
h4 {
    font-size: 15px;
	font-weight: 900
}}
</style>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <center><h4 class="m-b-10" style="text-transform: uppercase;">SIPIL (SISTEM INFORMASI PEMILU) KEPALA DAERAH KAB. KETAPANG 2020</h4></center>
                        </div>
                    </div>
                </div>
            </div>
        
        <!-- [ breadcrumb ] end -->
        <!-- [ Main Content ] start -->
        <div class="row">
            <div class=" col-md-12">
                <!-- support-section start -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card support-bar overflow-hidden">
							<div class="card-body">
                                <div class="row align-items-center">
                                    <div class="col-12">
									<center><img src="<?php echo base_url() ?>gambar/logomafan.png" width="70%"></center>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer bg-primary text-white">
                            </div>
                        </div>
                    </div>
                    
                </div>
                <!-- support-section end -->
            </div>
        </div>
        <!-- [ Main Content ] end -->
		</div>
    </div>
</div>