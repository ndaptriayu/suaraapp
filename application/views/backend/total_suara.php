<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- [ Main Content ] start -->
<section class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
		<div class="col-sm-12">
            <div class="card">
				<div class="card-body">
                <h5 class="m-b-10"><?php echo $title ?></h5>
                <hr>
					<div class="row">
                        <div class="col-md-12">
							<div class="container w-full md:w-4/5 xl:w-3/5  mx-auto px-2">
								<div id='recipients' class="p-8 mt-6 lg:mt-0 rounded shadow bg-white">

									<table id="example" class="table-striped text-center" style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
										<?php
											//untuk penomoran data
											$no=1;							
											//menampilkan data
											$query=$this->db->query("SELECT
											Sum(suara.paslon1+suara.paslon2+suara.paslon3+suara.paslon4) AS pstot,
											Sum(suara.paslon1) AS ps1,
											Sum(suara.paslon2) AS ps2,
											Sum(suara.paslon3) AS ps3,
											Sum(suara.paslon4) AS ps4,
											Sum(suara.tidaksah) AS tidaksah,
											Sum(suara.total_dptb) AS dptb,
											Sum(dpt.total_dpt) AS dpt,
											Sum(suara.total_dptb+dpt.total_dpt) AS dptot
											FROM
											suara ,
											dpt
											WHERE
											suara.id_tps = dpt.id_tps AND
											suara.id_kecamatan = dpt.id_kecamatan AND
											suara.id_keldes = dpt.id_keldes
											");
											foreach($query->result() as $hasil){
											$golput=$hasil->dptot-($hasil->pstot+$hasil->tidaksah)
										?>
										<tbody>
											<tr>
											  <th class="detail">No.</th>
											  <th class="detail">Nama Paslon</th>
											  <th class="detail">Total Suara</th>
											</tr>
											<tr>
											  <th class="detail">1</th>
											  <th class="detail">Iin-Rahmad</th>
												<td class="paslon"><?php echo number_format($hasil->ps1, 0, '.', '.'); ?></td>
											</tr>
											<tr>
											  <th class="detail">2</th>
											  <th class="detail">Junaidi-Sahrani</th>
												<td class="paslon"><?php echo number_format($hasil->ps2, 0, '.', '.'); ?></td>
											</tr>
											<tr>
											  <th class="detail">3</th>
											  <th class="detail">Eryanto-Mateus</th>
												<td class="paslon"><?php echo number_format($hasil->ps3, 0, '.', '.'); ?></td>
											</tr>
											<tr>
											  <th class="detail">4</th>
											  <th class="detail">Martin-Farhan</th>
												<td class="paslon"><?php echo number_format($hasil->ps4, 0, '.', '.'); ?></td>
											</tr>
											<tr>
												<th class="detail"></th>
												<th class="detail">Suara Sah</th>
												<td class="sah"><?php echo number_format($hasil->pstot, 0, '.', '.'); ?></td>
											</tr>
											<tr>
												<th class="detail"></th>
												<th class="detail">Tidak Sah</th>
												<td class="tidaksah"><?php echo number_format($hasil->tidaksah, 0, '.', '.'); ?></td>
											</tr>
											<tr>
												<th class="detail"></th>
												<th class="detail">Total DPT</th>
												<td class="total"><?php echo number_format($hasil->dpt, 0, '.', '.'); ?></td>
											</tr>
											<tr>
												<th class="detail"></th>
												<th class="detail">Total DPTB</th>
												<td class="total"><?php echo number_format($hasil->dptb, 0, '.', '.'); ?></td>
											</tr>
											<tr>
												<th class="detail"></th>
												<th class="detail">Total DPT+DPTB</th>
												<td class="total"><?php echo number_format($hasil->dptot, 0, '.', '.'); ?></td>
											</tr>
											<tr>
												<th class="detail"></th>
												<th class="detail">Total Golput</th>
												<td class="total"><?php echo number_format($golput, 0, '.', '.'); ?></td>
											</tr>
										</tbody>
									<?php } ?>	
									</table>						
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
        <!-- [ Main Content ] start -->
    </div>
</section>