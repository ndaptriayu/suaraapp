<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>SIPIL BUPATI KETAPANG 2020</title>
	<!-- HTML5 Shim and Respond.js IE11 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 11]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	<!-- Meta -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="description" content="" />
	<meta name="keywords" content="">
	<meta name="author" content="Phoenixcoded" />
	<!-- Favicon icon -->
	<link rel="icon" href="<?php echo base_url() ?>assets/images/avatar.png" type="image/x-icon">
	<!-- vendor css -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/style.css">
</head>
	<style>
	@media (max-width: 991px){
	.auth-wrapper{
		background-size: contain !important;
	}
	}
	.auth-wrapper{
		background: url(<?php echo base_url() ?>gambar/mafan.jpg) no-repeat;
		background-size: cover;
		background-attachment: fixed;
		background-size: 100% 100%;
		width: 100%;
		-webkit-background-size: cover;
		-moz-background-size: cover;
		-o-background-size: cover;
		-webkit-background-size: cover;
		-moz-background-size: cover;
		-o-background-size: cover;
	}
	html{
		position: relative;
		min-height: 100%;
	}
	.inifooter{
		background-color: #333;
		color: #fff;
		font-size: 14px;
		font-weight: bolder;
		position: absolute;
		bottom: 0;
		width: 100%;
		height: 50px;
		padding-top: 20px;
	}
	.btn-warning {
		color: #333;
		font-weight : bolder;
		background-color: #FFEB3B;
		border-color: #FFEB3B;
	}
	</style>
<body>
<!-- [ auth-signin ] start -->
<div class="auth-wrapper">
	<div class="auth-content">
		<div class="card">
			<div class="row align-items-center text-center">
				<div class="col-md-12">
				
					<div class="card-body" style="padding: 20px 25px;">
						<h5>SIPIL (SISTEM INFORMASI PEMILU)</h5>
						<h6>KEPALA DAERAH KAB. KETAPANG 2020</h6>
						<img src="<?php echo base_url() ?>gambar/logo-mafanQW.png" alt="" class="img-fluid mb-4">
						<?php echo $this->session->flashdata('notif') ?>
						<?php echo form_open_multipart('login/aksi_login') ?>
						<div class="form-group mb-4">
							<input type="text" class="form-control mb-3" placeholder="Username Anda" name="user" autofocus>
							<input type="password" class="form-control mb-3 form-password" placeholder="Password Anda" name="sandi">
							<input type="checkbox" class="form-checkbox"> Show password
						</div>
						<button type="submit" class="btn btn-block btn-warning">Login</button>
						<?php echo form_close() ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- [ auth-signin ] end -->

		<footer class="inifooter text-center">
			&copy; 
			<?php
				$fromYear = 2020; 
				$thisYear = (int)date('Y'); 
				echo $fromYear . (($fromYear != $thisYear) ? ' - ' . $thisYear : '');
			?>
			<?php echo $Footer ?>
		</footer>

<!-- Required Js -->
<script src="<?php echo base_url() ?>assets/js/vendor-all.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/ripple.js"></script>
<script type="text/javascript">
	$(document).ready(function(){		
		$('.form-checkbox').click(function(){
			if($(this).is(':checked')){
				$('.form-password').attr('type','text');
			}else{
				$('.form-password').attr('type','password');
			}
		});
	});
</script>
</body>
</html>